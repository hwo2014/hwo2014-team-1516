﻿namespace mono
{
	public class CarInfo
	{
		public RaceModel	RaceModel;

		public int		Id;
		public string	Name;
		public string	Color;
		public double	Width;
		public double	Length;
		public double	FlagPos;

		public CarInfo( RaceModel rm )
		{
			RaceModel = rm;
		}
	}
}
