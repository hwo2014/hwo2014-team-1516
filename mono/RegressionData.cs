﻿using System;
using System.Diagnostics;

namespace mono
{
	public class RegressionData
	{
		public int				Size;
		public Matrix			SumXX;
		public double[]			SumXY;
		public int				SumCount;
		public int				NextRecalc;
		public double[]			Coeffs;

		public RegressionData( int size )
		{
			Size = size;
			SumXX = new Matrix(size);
			SumXY = new double[size];
			SumCount = 0;
			NextRecalc = size;
		}

		public void AddData( double[] x, double y )
		{
			Debug.Assert( x.Length == Size );

			for( var i = 0; i < x.Length; i++ )
			{
				for( var j = 0; j < x.Length; j++ )
				{
					SumXX.Incr( i, j, x[i] * x[j] );
				}

				SumXY[i] += x[i] * y;
			}
			SumCount += 1;

			if( SumCount >= NextRecalc )
			{
				try
				{
					Coeffs = Calculate();
					Log.Info( "Recalc: " + Utils.ToString(Coeffs));
					NextRecalc *= 2;
				}
				catch( Exception ex )
				{
					Log.Info( "Failed to re-calculate: " + ex.Message );
				}
			}
		}

		public double[] Calculate()
		{
			// do regression now.
			var inv = SumXX.Invert();
			var coeffs = inv.Mul(SumXY);
			return coeffs;
		}

		public static void TestRegression()
		{
/*			var reg = new RegressionData(6);
			reg.AddData( new []{ 0.004954145,	0.004954145, 0.004954145, 6.063490672, 36.76591913,	0.030039415 }, 0.00359003  );
			reg.AddData( new []{ 0.00359003,	0.008544176, 0.013498321, 6.142220858, 37.72687707,	0.08290967  }, 0.003690286 );
			reg.AddData( new []{ 0.003690286,	0.012234461, 0.025732783, 6.183809687, 38.23950224,	0.159126631 }, 0.00350838  );
			reg.AddData( new []{ 0.00350838,	0.015742841, 0.041475624, 6.184641463, 38.24979003,	0.256511863 }, 0.003041492 );
			reg.AddData( new []{ 0.003041492,	0.018784334, 0.060259957, 6.184658099, 38.2499958,	0.372687234 }, 0.002592238 );
			reg.AddData( new []{ 0.002592238,	0.021376571, 0.081636529, 6.184658432, 38.24999992,	0.504894046 }, 0.002167758 );

			var coeffs = reg.Calculate();

			var v = new []{ -8.0254E-11, -0.1, -7.57311E-10, -0.005235988, 0.000975669, -0.00125 };

			for( int i = 0; i < 6; i++ )
				Debug.Assert( Math.Abs(coeffs[i] -  v[i])	< 0.0000001 );*/
		}
	}
}
