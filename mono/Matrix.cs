﻿using System;
using System.Diagnostics;

namespace mono
{
	public class Matrix
	{
		private readonly int		_size;
		private readonly double[,]	_m;

		public Matrix( int size )
		{
			_size = size;
			_m = new double[size, size];
		}

		public Matrix( Matrix other )
		{
			_size = other._size;
			_m = new double[_size, _size];
			Array.Copy( other._m, _m, _m.Length ); 
		}

		public Matrix( double[][] values )
		{
			_size = values.Length;
			_m = new double[_size, _size];

			for( var i = 0;i < _size; i++ )
			{
				if( values[i].Length != _size )
					throw new Exception("Maxtrices must be square");

				for( var j = 0; j < _size; j++ )
					_m[i, j] = values[i][j];
			}
		}

		public bool Equals( Matrix other )
		{
			if( _size != other._size )
				return false;

			for( int i = 0; i < _size; i++ )
			{
				for( int j = 0; j < _size; j++ )
				{
					if( Math.Abs(_m[i, j] - other._m[i, j]) > 1E-6 )
						return false;
				}
			}

			return true;
		}

		public double Get( int row, int col )
		{
			return _m[row, col];
		}

		public void Set( int row, int col, double value )
		{
			_m[row, col] = value;
		}

		public void Incr( int row, int col, double value )
		{
			_m[row, col] += value;
		}

		public void SetIdentity()
		{
			for( var i = 0; i < _size; i++ )
				for( var j = 0; j < _size; j++ )
					_m[i, j] = i == j ? 1 : 0;
		}

		public double[] Mul( double[] values )
		{
			var result = new double[_size];

			for( var i = 0; i < _size; i++ )
			{
				for( var j = 0; j < _size; j++ )
				{
					result[i] += _m[i, j] * values[j];
				}
			}

			return result;
		}

		public static void TestSimultanenousEqnSolver()
		{
			var	m = new Matrix( new []{ new []{ -0.003563008, -0.117092477, 4.583651983 },
										new []{ -0.022668142, -0.13976062,  4.443891364 },
										new []{ -0.019353123, -0.159113743, 4.284777621 } } );

			var v = new []{ -0.022668142, -0.019353123, -0.016224458 };

			var result = m.GaussJordan(v);

			Debug.Assert( Math.Abs(result[0] -  6.97964E-12)	< 0.0000001 );
			Debug.Assert( Math.Abs(result[1] - -0.1)			< 0.0000001 );
			Debug.Assert( Math.Abs(result[2] - -0.0075)			< 0.0000001 );

			m = new Matrix( new []{ new []{ 0.058089349, -0.972839419, -23.30052639, 6 }, 
									new []{ 0.292377518, -0.6804619,   -23.98098829, 6 }, 
									new []{ 0.268243231, -0.41221867,  -24.39320696, 6 }, 
									new []{ 0.244510548, -0.167708122, -24.56091508, 6 } } );

			v = new []{ 0.292377518, 0.268243231, 0.244510548, 0.221317304 };

			result = m.GaussJordan(v);

			Debug.Assert( Math.Abs(result[0] - -1.07075E-11)	< 0.0000001 );
			Debug.Assert( Math.Abs(result[1] - -0.1)			< 0.0000001 );
			Debug.Assert( Math.Abs(result[2] - -0.0075)			< 0.0000001 );
			Debug.Assert( Math.Abs(result[3] -  0.003389938 )	< 0.0000001 );

			m = new Matrix( new []{ new []{ -0.002573, -0.002573, 32.646927, 5.713749 }, 
									new []{ -0.004969, -0.007542, 32.829284, 5.729684 }, 
									new []{ -0.00719 , -0.014731, 33.008707, 5.74532  }, 
									new []{ -0.009233, -0.023964, 33.185234, 5.760663 }, } );

			v = new []{ -0.002397, -0.00222, -0.002044, -0.001868 };

			result = m.GaussJordan(v);

			Debug.Assert( Math.Abs(result[0] -  0.077873986)	< 0.0000001 );
			Debug.Assert( Math.Abs(result[1] -  0.004277027)	< 0.0000001 );
			Debug.Assert( Math.Abs(result[2] -  0.004281468)	< 0.0000001 );
			Debug.Assert( Math.Abs(result[3] - -0.024845756)	< 0.0000001 );
		}

		public static void TestMatrixInversion()
		{
			Matrix m;
			m = new Matrix( new []{ new []{ 0.058089349, -0.972839419, -23.30052639, 6 }, 
									new []{ 0.292377518, -0.6804619,   -23.98098829, 6 }, 
									new []{ 0.268243231, -0.41221867,  -24.39320696, 6 }, 
									new []{ 0.244510548, -0.167708122, -24.56091508, 6 } } );
			var i = m.Invert();

			Debug.Assert( i.Equals(new Matrix( new []{	new []{ -3.865965972, 11.99523322,  -12.42478486,   4.295517611 },
														new []{ -0.397335394,  4.238082801, -11.66896819,   7.828220781 },
														new []{ -0.03221639,   4.481457907,  -9.291812022,  4.842570504 },
														new []{  0.014561512, 17.97441783,  -37.85573263,  20.03341996  } } )) );
/*
			// this one seems to fall over the "Matrix is singular!" check.
			m = new Matrix( new []{ new []{  0.0083597300904928139,  0.05243629257474032, -1.1857406939262078, -7.6891233966542769 },
									new []{  0.05243629257474032,    0.33842475122317339, -7.4259923884916468, -48.155024205723315 },
									new []{ -1.1857406939262078,    -7.4259923884916468,   168.20318025222406,  1090.7401721927583 },
									new []{ -7.6891233966542769,    -48.155024205723315,   1090.7401721927583,  7073.0774617405441 } } );

			i = m.Invert();

			Debug.Assert( i.Equals(new Matrix( new []{	new []{  5333258.717, -43924.61545,	 3732.577632,  4923.118904 },
														new []{ -43924.61545,  456.3245124,	-29.46337816, -40.10008936 },
														new []{  5220.592422, -41.59669148, -1.48451E+12,  2.28926E+11 },
														new []{  4693.654744, -38.22894482,  2.28926E+11, -35302717250 } } )) );
*/
		}

		public static void Test()
		{
			try
			{
				TestSimultanenousEqnSolver();
				TestMatrixInversion();
			}
			catch( Exception ex )
			{
				Log.Info("Test: " + ex.Message );
			}
		}

		public double[] GaussJordan( double[] x )
		{
			// gauss-jordan...
			// based on pseudo code from: http://en.wikipedia.org/wiki/Gaussian_elimination

			if( x.Length != _size )
				throw new Exception("Size mismatch between matrix and vector!");

			// take copies, as they're going to get blatted.
			var a = new Matrix(this);
			var b = new double[x.Length];
			Array.Copy(x, b, x.Length);

			for( var k = 0; k < _size; k++ )
			{
				// Find pivot for column k:
				var iMax = k;
				var max = Math.Abs(a._m[k, k]);
				for( int i = k + 1; i < _size; i++ )
				{
					var n = Math.Abs(a._m[i, k]);
					if( max < n )
					{
						iMax = i;
						max = n;
					}
				}

				if( a._m[iMax, k] == 0 )
					throw new Exception("Solve: Matrix is singular!");

				// swap rows(k, i_max)
				for( var i = 0; i < _size; i++ )
					Utils.Swap( ref a._m[k, i], ref a._m[iMax, i] );
				Utils.Swap( ref b[k], ref b[iMax] );

				// Do for all rows below pivot:
				for( var i = k + 1; i < _size; i++ )
				{
					// Do for all remaining elements in current row:
					var scale = a._m[i, k] / a._m[k, k];
					for( var j = k + 1; j < _size; j++ )
					{
						a._m[i, j] -= a._m[k, j] * scale;
					}
					b[i] -= b[k] * scale;

					// Fill lower triangular matrix with zeros:
					a._m[i, k] = 0;
				}
			}

			// now just need to do back substitution to find values...

			var v = new double[_size];
/*
			v[2] = b[2] / a._m[2, 2];
			v[1] = (b[1] - a._m[1, 2] * v[2]) / a._m[1, 1];
			v[0] = (b[0] - a._m[0, 2] * v[2] - a._m[0, 1] * v[1]) / a._m[0, 0];
*/
			for( var i = _size - 1; i >= 0; i-- )
			{
				var n = b[i];
				for( var j = i + 1; j < _size; j++ )
					n -= a._m[i, j] * v[j];
				v[i] = n / a._m[i, i];
			}

			return v;
		}

		public Matrix Invert()
		{
			// gauss-jordan...
			// based on pseudo code from: http://en.wikipedia.org/wiki/Gaussian_elimination

			// take copies, as they're going to get blatted.
			var a = new Matrix(this);
			var b = new Matrix(_size);
			b.SetIdentity();

			for( var k = 0; k < _size; k++ )
			{
				// Find pivot for column k:
				var iMax = k;
				var max = Math.Abs(a._m[k, k]);
				for( var i = k + 1; i < _size; i++ )
				{
					var n = Math.Abs(a._m[i, k]);
					if( max < n )
					{
						iMax = i;
						max = n;
					}
				}

				if( a._m[iMax, k] == 0 )
					throw new Exception("Invert: Matrix is singular!");

				// swap rows(k, i_max)
				for( var i = 0; i < _size; i++ )
				{
					Utils.Swap( ref a._m[k, i], ref a._m[iMax, i] );
					Utils.Swap( ref b._m[k, i], ref b._m[iMax, i] );
				}

				// Do for all rows below pivot:
				for( var i = k + 1; i < _size; i++ )
				{
					// Do for all remaining elements in current row:
					var scale = a._m[i, k] / a._m[k, k];
					for( var j = k + 1; j < _size; j++ )
						a._m[i, j] -= a._m[k, j] * scale;
					for( var j = 0; j < _size; j++ )
						b._m[i, j] -= b._m[k, j] * scale;

					// Fill lower triangular matrix with zeros:
					a._m[i, k] = 0;
				}
			}

			// now need to do back substitution...
			// work backwards from the bottom row.
			for( var i = _size - 1; i >= 0; i-- )
			{
				// want to make a[i, i] be 1.
				var scale = 1.0 / a._m[i, i];
				for( var j = i + 1; j < _size; j++ )
					a._m[i, j] *= scale;
				for( var j = 0; j < _size; j++ )
					b._m[i, j] *= scale;
				a._m[i, i] = 1.0;

				// make the rest of the row a[i, ..] bo 0.
				for( var j = i + 1; j < _size; j++ )
				{
					scale = a._m[i, j];
					a._m[i, j] -= a._m[j, j] * scale;
					for( var k = 0; k < _size; k++ )
						b._m[i, k] -= b._m[j, k] * scale;
				}
			}

			return b;
		}

		public Matrix Transpose()
		{
			var m = new Matrix(_size);

			for( var i = 0; i < _size; i++ )
				for( var j = 0; j < _size; j++ )
					m._m[j, i] = _m[i, j];

			return m;
		}

		public override string ToString()
		{
			var s = string.Empty;
			for( var row = 0; row < _size; row++ )
			{
				for( var col = 0; col < _size; col++ )
				{
					s += ", " + _m[row, col].ToString("R");
				}
				s += "\r\n";
			}
			return s;
		}
	}
}
