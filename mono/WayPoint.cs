﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mono
{
	public class WayPoint
	{
		public TrackPoint	Tp;
		public double		Speed;

		public WayPoint( TrackPoint tp, double speed )
		{
			Tp = tp;
			Speed = speed;
		}
	}

}
