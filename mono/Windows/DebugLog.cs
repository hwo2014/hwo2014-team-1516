﻿using System.Diagnostics;

//using System.;

namespace mono.Windows
{
	class DebugLog : Log
	{
		public override void DoInfo(string msg)
		{
#if DEBUG
			Debug.WriteLine( msg );
#else
			Console.WriteLine( msg );
#endif
		}
	}
}
