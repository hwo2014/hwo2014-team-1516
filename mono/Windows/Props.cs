﻿using System;
using System.Collections.Specialized;

namespace mono.Windows
{
	class Props
	{
		readonly Func<StringCollection>		_getter;
		readonly Action<StringCollection>	_setter;

		public Props( Func<StringCollection> getter, Action<StringCollection> setter )
		{
			_getter = getter;
			_setter = setter;
		}

        public string LoadSetting( string key )
        {
            var settings = _getter.Invoke() ?? new StringCollection();

            for( var i = 0; i < settings.Count; i++ )
            {
                if( settings[i].StartsWith(key + ":") )
                {
                    return settings[i].Substring((key + ":").Length);
                }
            }

            return null;
        }

        public void SaveSetting( string key, string value )
        {
            var settings = _getter.Invoke() ?? new StringCollection();

			bool add = true;
            for( var i = 0; i < settings.Count; i++ )
            {
                if( settings[i].StartsWith(key + ":") )
                {
                    settings[i] = key + ":" + value;
					add = false;
                    break;
                }
            }

            if( add )
				settings.Add( key + ":" + value );

            _setter.Invoke( settings );
            Properties.Settings.Default.Save();
        }

		public string GetString( string key, string defValue )
		{
			return LoadSetting(key) ?? defValue;
		}

		public void SetString( string key, string value )
		{
			SaveSetting( key, value );
		}

		public int GetInt( string key, int defValue )
		{
			var strValue = LoadSetting(key);
			if( strValue == null )
				return defValue;

			return int.Parse(strValue);
		}

		public void SetInt( string key, int value )
		{
			SaveSetting( key, value.ToString() );
		}

		public bool GetBool( string key, bool defValue )
		{
			var strValue = LoadSetting(key);
			if( strValue == null )
				return defValue;

			return bool.Parse(strValue);
		}

		public void SetBool( string key, bool value )
		{
			SaveSetting( key, value.ToString() );
		}
	}
}
