﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace mono.Windows
{
    public class FormUtils
    {
		private static readonly Props	_props = new Props(	() => Properties.Settings.Default.FormSettings,
															x  => Properties.Settings.Default.FormSettings = x );

        public static void CenterForm( Form form, Form parent )
        {
            form.Location = new Point(
                parent.Location.X + (parent.Width - form.Width) / 2,
                parent.Location.Y + (parent.Height- form.Height) / 2);
        }

        public static void SaveLocation( Form form )
        {
            SaveLocation( form.GetType().Name, form );
        }
        
        public static void SaveLocation( string name, Form form )
        {
            if( form.WindowState != FormWindowState.Normal )
                return;

            var bounds = form.DesktopBounds;
            var state = form.WindowState;

            var value = bounds.Location.X + ":" +
                        bounds.Location.Y + ":" +
                        bounds.Size.Width + ":" + 
                        bounds.Size.Height + ":" +
                        (int)state;

            _props.SaveSetting( "Form/" + name, value );
        }

        public static void RestoreLocation( Form form )
        {
            RestoreLocation( form.GetType().Name, form );
        }
        
        public static void RestoreLocation( string name, Form form )
        {
            var value = _props.LoadSetting("Form/" + name);
            if( value == null )
                return;

            //var items = settings[index].Split(':');
            var items = value.Split(':');
            try
            {
                var x = int.Parse(items[0]);
                var y = int.Parse(items[1]);
                var w = int.Parse(items[2]);
                var h = int.Parse(items[3]);
                var s = (FormWindowState)int.Parse(items[4]);

                form.DesktopBounds = new Rectangle(x, y, w, h);
                form.WindowState = s;
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
                // quietly fail here.
            }
        }

        public static void RestoreSize( Form form )
        {
            RestoreSize( form.GetType().Name, form );
        }
        
        public static void RestoreSize( string name, Form form )
        {
            var value = _props.LoadSetting("Form/" + name);
            if( value == null )
                return;
                
            //var items = settings[index].Split(':');
            var items = value.Split(':');
            try
            {
                var w = int.Parse(items[2]);
                var h = int.Parse(items[3]);
                var s = (FormWindowState)int.Parse(items[4]);

                form.Size = new Size(w, h);
                CenterForm( form, form.Owner );
                form.WindowState = s;
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
                // quietly fail here.
            }
        }
        
        public static void SaveSplitters( Form form )
        {
            SaveSplitters( form.GetType() + "/Splitters", form );
        }
        
        private static void SaveSplitters( String id, Control control )
        {
            foreach( Control c in control.Controls )
            {
                if( c as SplitContainer != null )
                {
                    // found a splitter
                    var s = c as SplitContainer;
                    var name = id + "/" + control.GetType().Name + "/" + s.Name;
                    _props.SaveSetting( name, s.SplitterDistance.ToString() );
                }
                
                if( c.Controls.Count != 0 )
                    SaveSplitters( id, c );
            }
        }
        
        public static void RestoreSplitters( Form form )
        {
            RestoreSplitters( form.GetType() + "/Splitters", form );
        }

        private static void RestoreSplitters( string id, Control control )
        {
            foreach( Control c in control.Controls )
            {
                if( c as SplitContainer != null )
                {
                    // found a splitter
                    var s = c as SplitContainer;
                    var name = id + "/" + control.GetType().Name + "/" + s.Name;

                    var value = _props.LoadSetting(name);
                    if( value != null )
                    {
                        try
                        {
                            var split = int.Parse(value);
                            s.SplitterDistance = split;
                        }
                        // ReSharper disable EmptyGeneralCatchClause
                        catch (Exception)
                        // ReSharper restore EmptyGeneralCatchClause
                        {
                            // quietly fail here.
                        }
                    }
                }
                
                if( c.Controls.Count != 0 )
                    RestoreSplitters( id, c );
            }
        }
    }
}
