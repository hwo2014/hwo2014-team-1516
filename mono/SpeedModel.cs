﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace mono
{
	public class SpeedModel
	{
		public double	ThrottleK	{ get; private set; }
		public double	AccelK		{ get; private set; }

		/// <summary>
		/// Default speed model for use while we have no data.
		/// </summary>
		public SpeedModel()
		{
			// these are the values from the Keimola (Finland) track.
			ThrottleK = 10;
			AccelK = 49;
		}

		// assumes v=0 @ t=0, v=U @ t=1, v=V @ t=2, and throttle used is constant.
		public SpeedModel( double U, double V, double throttle )
		{
			Debug.Assert( U < V );
			Debug.Assert( U > 0 );
			Debug.Assert( 2 * U > V );

			if( U >= V || U <= 0 || 2 * U <= V )
			{
				ThrottleK = 10;
				AccelK = 49;
				return;
			}

			double limit = CalcLimit(U, V);
			ThrottleK	= limit / throttle;
			AccelK		= (ThrottleK - V) / (V - U);
		}

		//	AccelK		= (ThrottleK - V) / (V - U);
		//	AccelK * (V - U) = Tk - V
		//	AccelK * V - AccelK * U = Tk - V
		//	AccelK * V + V = Tk + Ak * U
		//	V(AccelK + 1) = Tk + Ak * U
		//	V = (Tk + Ak * U) / (AccelK + 1)

		public double ApplyThrottle( double speed, double throttle )
		{
			throttle = Math.Max(0, Math.Min(1, throttle));
//			return speed + (throttle * ThrottleK - speed) / AccelK;
			
			return (throttle * ThrottleK + AccelK * speed) / (AccelK + 1);
		}

		// Based on the RC charging equation: Vcap(t) = Vin(1 - e ^ (-t/RC))
		//
		// given two speeds, U and V, where U is from t=1 and V is from t=2, and start (v=0) was at t=0,
		// and the throttle has been set at a constant non-zero value, calculates the limit speed (at t=inf.)
		//
		//	U = v (1 - e ^ x)		// t = 1
		//
		//	V = v (1 - e ^ 2x)		// t = 2
		//	V = v (1 - (e ^ x) ^ 2)
		//
		//	let E = e ^ x
		//
		//	U = v (1 - E)		
		//	V = v (1 - E*E)
		//
		//	v = U / (1 - E)
		//	v = V / (1 - E*E)
		//
		//	v/v = [U / (1 - E)] / [V / (1 - E*E)]
		//	 1  = (U / V) * (1 - E*E) / (1 - E)
		//
		//	let K = U / V
		//
		//	K = (1 - E) / (1 - E*E)
		//	K(1 - E*E) = 1 - E
		//	K - K E*E - 1 + E = 0
		//	K E*E - E + (1 - K) = 0
		//
		//	E = 1 or E = 0.98
		//	v = U / (1 - E)

		public static double CalcLimit( double U, double V )
		{
			double K = U / V;
			// K E*E - E + (1 - K) = 0
			//		a = K
			//		b = -1
			//		c = 1 - K
			// E =  -b +- sqrt(b*b - 4ac) / (2a)
			double E0 = (1 - Math.Sqrt(1 - 4 * K * (1 - K))) / (2 * K);
			//double E1 = (1 + Math.Sqrt(1 - 4 * K * (1 - K))) / (2 * K);

			double	v = U / (1 - E0);
			return v;
		}

		public double ThrottleForSpeed( double targetSpeed, double currentSpeed )
		{
			var throttle = ((targetSpeed - currentSpeed) * AccelK + currentSpeed) / ThrottleK;
			return Math.Max(0, Math.Min(1, throttle));
		}

		public double TargetSpeed( double distToGo, double finalSpeed )
		{
			// v' = (targV + AccelK * u) / (AccelK + 1);
			// v' * (AccelK + 1) = targV + AccelK * u
			// u = (v' * (AccelK + 1) - targV) / AccelK

			// work this equation backwards:
			//		v' = u + (targV - u) / AccelK;
			// when braking, targV = 0
			//		v' = u - u / AccelK
			//		v' = u(1 - 1 / AccelK)
			//		u = v' / (1 - 1 / AccelK)

			var v = finalSpeed;
			var u = v;
			while( distToGo > 0 )
			{
				u = v;
//				v = v / (1 - 1 / AccelK);
				var x = v / (1 - 1 / AccelK);
				v = (v * (AccelK + 1)) / AccelK;
				distToGo -= v;
			}

			return u;
		}
	}
}
