﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace mono
{
	public class RegressionDriftModel : IDriftModel
	{
		private readonly List<Dictionary<double, RegressionData>>	Regs = new List<Dictionary<double, RegressionData>>();

		public const int		Items = 6;

		public double			FrictionK1 = 0.0006;	// low value, hopefully fairly safe.
		public double			FrictionK2 = 0.0006;	// low value, hopefully fairly safe.

		/// from analysing the data from initial angles after straights, at various speeds
		/// and with various radii, this equation has been derived:
		///
		/// angle = (3.7 / (R + 20) + 1/45) * V*V - 0.3 V
		///   
		/// for the data see docs/angular-rotation-analysis.xls
		/// 
		private double SimpleAngleCoef = 3.7;
		public double	AngAccelDegrees( double R, double V )
		{
//			var angAcc = (3.7 / (R + 20) + 1.0/45) * V * V - 0.3 * V;
			var angAcc = (SimpleAngleCoef / (R + 20) + 1.0/45) * V * V - 0.3 * V;
			return Math.Max(0, angAcc);
		}

		public RegressionData GetRegression( int piece, double radius )
		{
			var reg = FindRegression(piece, radius);
			if( reg == null )
			{
				reg = new RegressionData(Items);
				SetRegression( piece, radius, reg );
			}

			return reg;
		}

		public void SetRegression( int piece, double radius, RegressionData reg )
		{
			while( piece >= Regs.Count  )
				Regs.Add( new Dictionary<double, RegressionData>() );

			if( !Regs[piece].ContainsKey(radius) )
				Regs[piece].Add( radius, null );

			Regs[piece][radius] = reg;
		}

		private RegressionData FindRegression( int piece, double radius )
		{
			if( piece >= Regs.Count  )
				return null;

			if( !Regs[piece].ContainsKey(radius) )
				return null;

			return Regs[piece][radius];
		}

		// called before the car has been updated with new position data, so all the members still have
		// the data from the previous tick.  The gameTick, newIndex and newRotAcc values passed in are
		// from the new tick.
		public void NewData( int gameTick, Car car, int newIndex, double newRotAcc )
		{
			if( !car.IsRunning || car.Index != newIndex )
			{
				// ignore if car is not running, or we just moved to a new piece (as the speed is invalid.)
				return;
			}

			{
				// Simple drift model:
				// * try to learn the FrictionK value for the track.
				var angle  = car.Angle;
				var rotSpd = car.AngSpeed;
				var rotAcc = car.AngAccel;
				ApplySimpleDriftCalculation( car.Index, car.Radius(), car.Speed, ref angle, ref rotSpd, ref rotAcc );
				var learnRate = 0.0001;
				var R = car.Radius();
				if( R == 0 )
					FrictionK1 += learnRate * (angle - car.Angle);
				else if( R > 0 )
					FrictionK2 += learnRate * (angle - car.Angle);
				else
					FrictionK2 -= learnRate * (angle - car.Angle);
			}

//			if( isvaliddata(car) )
			if( car.Speed > 3 && car.SpeedValid && car.Angle != 0 && car.AngSpeed != 0 )
			{
				if( car.Index == 27 )
					car.Index += 0;

//				var x = new []{ 1.0, car.AngAccel, car.AngSpeed, car.Angle, car.Speed };
				var x = new []{ car.AngAccel, car.AngSpeed, car.Angle,
								car.Speed, car.Speed * car.Speed, car.Speed * car.Angle };

				Log.Info( newRotAcc + " <-- " + Utils.ToString(x) );
				Debug.Assert( x.Length == Items );

				var rd = GetRegression(car.Index, car.Radius());
				rd.AddData( x, newRotAcc );
/*
				var radius = car.Radius();
				if( radius != 0 )
				{
					var angErr = car.Angle - car.SimAngle;
					if( radius > 0 )
						SimpleAngleCoef += angErr * 0.1;
					else
						SimpleAngleCoef -= angErr * 0.1;
				}*/
			}
		}

		private  bool ApplySimpleDriftCalculation(
			int piece, double radius, double speed,
			ref double angle, ref double rotSpd, ref double rotAcc )
		{
			var curvature = radius == 0 ? 0 : 1 / radius;

			var latAcc = speed * speed * curvature;

			// variables affecting the rotation of the car:
			//	* friction torque related to angle of the side of the car to the
			//	  direction of motion.
			//  * lateral acceleration torque acting on the centre of mass.
			//	* rotational inertia/speed??
					
			// from tests it seems that theres no sliding force for lateral accelerations of less than 0.32
			//throttle = 0.5368;	// just slides,  90 rad, v = 5.356, a = 0.3202
			//throttle = 0.5935;	// just slides, 110 rad, v = 5.935, a = 0.3202
			//throttle = 0.3578;	// just slides,  40 rad, v = 3.578, a = 0.32005
			//throttle = 0.4382;	// just slides,  60 rad, v = 4.382, a = 0.32003
			if( latAcc > 0.32 )
				latAcc -= 0.32;
			else if( latAcc < -0.32 )
				latAcc += 0.32;
			else
				latAcc = 0;

			//var accTorque  = latAcc * Math.Cos(angle);
			var accTorque  = latAcc;// * angle;
			//var fricTorque = -speed * Math.Sin(angle);
			var fricTorque = -speed * angle;

			// best so far.
			//var KA = 0.06;
			//var KF = 0.00125;

			var R = radius;
			////var KA = 0.055;
			////var KA = 0.045;	// smaller for german, rad=40
			//var KA = GetAccelCoef(radius);	// smaller for german, rad=40
//			var KF = 0.00125;
//			var KF = 0.001275;
			var KF = R == 0 ? FrictionK1 : FrictionK2;
//			var rotAcc = KA * accTorque + KF * fricTorque;

			rotAcc = 0.0;
			if( R > 0 )
				rotAcc = Utils.Radians(AngAccelDegrees(R, speed));
			else if( R < 0 )
				rotAcc = Utils.Radians(-AngAccelDegrees(-R, speed));
			rotAcc += KF * fricTorque;

			rotSpd += rotAcc;
			angle  += rotSpd;
			rotSpd *= 0.9;

			return true;
		}

		public bool Apply(
			int piece, double radius, double speed,
			ref double angle, ref double rotSpd, ref double rotAcc )
		{
			var reg = GetRegression(piece, radius);
			if( reg == null || reg.Coeffs == null )
//			if( true )
			{
				// no data yet, so use a default simple simulation.
				return ApplySimpleDriftCalculation( piece, radius, speed, ref angle, ref rotSpd, ref rotAcc );
			}

			var x = new []{ rotAcc, rotSpd, angle, speed, speed * speed, angle * speed };

//			if( radius == 0 )
//				x = new []{ rotAcc, rotSpd, angle, angle * speed };

			var coeffs = reg.Coeffs;

			var sum = 0.0;
			for( int i = 0; i < coeffs.Length && i < x.Length; i++ )
			{
				sum += coeffs[i] * x[i];
			}
			rotAcc = sum;
/*
//			if( rotSpd == 0 && angle == 0 && speed * speed / Math.Abs(radius) < 0.32 )
			if( radius == 0 )
			{
				// straights have no lateral acceleration.
				rotAcc = rotSpd * coeffs[0] + angle * coeffs[1];// + speed  * coeffs[2] + speed * speed * coeffs[3];
			}
			else
			{
				if( true )//speed * speed / Math.Abs(radius) > 0.32 )
				{
					rotAcc = rotSpd * coeffs[0] + angle * coeffs[1] + speed * coeffs[2] + speed * speed * coeffs[3];
				}
				else
				{
					rotAcc = rotSpd * coeffs[0] + angle * coeffs[1];// + speed * coeffs[2] + speed * speed * coeffs[3];
				}
			}
*/
			if( rotAcc != 0 )
				rotAcc *= 1;

			rotSpd += rotAcc;
			angle  += rotSpd;

			if( Math.Abs(angle) > Math.PI / 2 )
			{
				angle  = Math.Sign(angle) * Math.PI / 2;
				rotSpd = 0;
			}

			return true;
		}

		public string GetUiString()
		{
			var info = string.Empty;
			info += string.Format("FrictionK1: {0:f6}\r\n", FrictionK1 );
			info += string.Format("FrictionK2: {0:f6}\r\n", FrictionK2 );
			return info;
		}

		public void DumpCoefficients()
		{
			for( var i = 0; i < Regs.Count; i++ )
			{
				var regs = Regs[i];
				if( regs.Count != 0 )
				{
					var values = string.Empty;
					foreach( var entry in regs )
					{
						values += " " + entry.Key + ":";
						try
						{
							var coeffs = entry.Value.Calculate();
							foreach( var value in coeffs )
								values += " " + value;
						}
						catch( Exception ex )
						{
							values = ex.Message;
						}
						Log.Info( "accel[" + i + "] =" + values );
					}
				}
			}
		}

		public bool GotData( int index, double radius, double speed )
		{
			throw new NotImplementedException();
		}
	}
}
