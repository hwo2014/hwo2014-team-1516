﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mono
{
	public class ApexInfo
	{
		public TrackPoint	Start;
		public TrackPoint	End;
		public double		Fraction;
		public double		MinSpeed;
		public double		MaxSpeed;
		public double		Speed;

		public WayPoint CreateWayPoint( int pieceCount )
		{
			var range = Start.RangeTo(End, pieceCount);
			var tp = Start.PlusRange(range * Fraction, pieceCount);
			return new WayPoint(tp, Speed);
		}
	}
}
