using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using mono.Drivers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace mono
{
	public class Bot
	{
		public string			Filename	{ get; set; }	// only if running from previously saved data.
		public string			Track		{ get; set; }


		public bool				CreateRace	{ get; set; }
		public string			Password	{ get; set; }

		public int				BotIndex	{ get; set; }
		public string			BotKey      { get; set; }
		public string			BotName     { get; set; }
		public int				NumBots		{ get; set; }
		public int				Port        { get; set; }
		public string			Host        { get; set; }

		public bool				IsJoined	{ get; private set; }

		private RaceModel       RaceModel   { get; set; }
		private SpeedModel		SpeedModel	{ get; set; }

		private StreamReader    Reader      { get; set; }
		private StreamWriter    Writer      { get; set; }

		private int				GameTick	{ get; set; }
		private PathRunner[]	LaneRunners { get; set; }

		public	Type			DriverType  { get; set; }
		private DriverBase		Driver		{ get; set; }

		private bool			Simulating = false;
//		private bool			Simulating = true;

		private double[]		SpeedModelData = new double[6];

		public static void Main(string[] args)
		{
			Log.Logger = new ConsoleLog();
        
			if( args.Length != 1 && args.Length != 4 )
			{
				Log.Info( "usage: bot.exe <host> <port> <botName> <botKey>" );
				Log.Info( "   or: bot.exe <json-log-filename>" );
				return;
			}

			Bot bot;
			var driverType = typeof(SimLookaheadDriver2);

			if( args.Length == 1 )
			{
				bot = new Bot(args[0], driverType, new RaceModel());
			}
			else
			{
				var host    = args[0];
				var port    = int.Parse(args[1]);
				var botName = args[2];
				var botKey  = args[3];
        
				bot = new Bot(host, port, null, 0, botName, botKey, null, false,
							  0, driverType, new RaceModel());
			}

			bot.run();
		}

		/// <summary>
		/// This constructor creates or joins a race.
		/// If the botIndex is 0 it will create and join a race.
		/// If the botIndex is not 0, it will only join a race.
		/// </summary>
		public Bot(
			string host, int port, string track, int botIndex,
			string botName, string botKey,
			string password, bool createRace, int raceBots,
			Type driverType, RaceModel rm )
		{
			Host        = host;
			Port        = port;
			CreateRace	= createRace;
			Track		= track;
			Password	= password;
			BotIndex	= botIndex;
			BotName     = botName;
			BotKey      = botKey;
			DriverType	= driverType;
			NumBots		= raceBots;
			RaceModel   = rm;
			SpeedModel	= new SpeedModel();
		}
/*
	/// <summary>
	/// This constructor attempts to join a previously created race.
	/// </summary>
	public Bot( string host, int port, string track, string botName, string botKey, RaceModel rm )
	{
	    Host        = host;
	    Port        = port;
		Track		= track;
	    BotName     = botName;
	    BotKey      = botKey;
	    RaceModel   = rm;
	}
*/
		public Bot( string jsonInputFilename, Type driverType, RaceModel rm )
		{
			Filename	= jsonInputFilename;
			RaceModel   = rm;
			DriverType	= driverType;
			SpeedModel	= new SpeedModel();
		}

		public void Info( string msg )
		{
			Log.Info( "[" + BotName + ":" + GameTick + "] " + msg );
		}

		public void run()
		{    
			try
			{
				if( Filename != null )
				{
					Info( @"Replaying from log file: " + Filename );

					using(var reader = new StreamReader(new FileStream(Filename, FileMode.Open)))
					{
						var writer = new StreamWriter(Stream.Null);

						Reader = reader;
						Writer = writer;

						runLoop(new Join("Recording", "NoKeyRequired"));
					}
				}
				else
				{
					Info( @"Connecting to " + Host + @":" + Port + @" as " + BotName + @"/" + BotKey );

					using(var client = new TcpClient(Host, Port))
					{
						var stream = client.GetStream();
						var reader = new StreamReader(stream);
						var writer = new StreamWriter(stream);
						writer.AutoFlush = true;

						Reader = reader;
						Writer = writer;

						runLoop(
							Track == null ?
								(SendMsg)new Join(BotName, BotKey) :
							CreateRace && BotIndex == 0 ?
								(SendMsg)new CreateRace(Track, Password, BotName, BotKey, NumBots) :
								(SendMsg)new JoinRace(Track, Password, BotName, BotKey, NumBots));

						reader.Close();
						writer.Close();
						client.Close();
					}
				}
			}
			catch( Exception ex )
			{
				Info( ex.Message );
				Info( ex.StackTrace );
			}
		}
		
		public void stop()
		{
			if( Reader != null )
				Reader.Close();
		}

		private void runLoop( SendMsg joinMsg )
		{
			string line;

			send( joinMsg );
			Info( "Sent join msg: " + joinMsg.ToJson() );

			Car Sim = null;

			while( (line = Reader.ReadLine()) != null )
			{
				Log.Json( line );

				var msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
				//Info( "Received msg: \"" + msg.msgType + "\": " +
				//		(msg.data == null ? "null" : msg.data.GetType().ToString()) );

				switch(msg.msgType)
				{
					case "join":
						Info( "Joined");
						send(new Ping());
						IsJoined = true;
						GameTick = -2;
						break;

					case "createRace":
						Info( "Received msg: \"" + msg.msgType + "\": " + msg.data );
						//send( new JoinRace(Track, "RatRacersOnly!", BotName, BotKey, NumBots) );
						send(new Ping());
						IsJoined = true;
						GameTick = -2;
						break;

					case "joinRace":
						Info( "Received msg: \"" + msg.msgType + "\": " + msg.data );
						send(new Ping());
						IsJoined = true;
						GameTick = -2;
						break;

					case "yourCar":
						{
							var rcvData = msg.data as JObject;
							RaceModel.MeName  = (string)rcvData["name"];
							RaceModel.MeColor = (string)rcvData["color"];
							Info(">>> " + RaceModel.MeName + ", " + RaceModel.MeColor);

							break;
						}

					case "gameInit":
						Info("Race init");
						RaceModel.GameInit( msg.data as JObject );
						LaneRunners = new PathRunner[RaceModel.Lanes.Count];
						for( int i = 0; i < LaneRunners.Length; i++ )
							LaneRunners[i] = new PathRunner(RaceModel.LanePaths[i]);
						send(new Ping());
						break;

					case "gameStart":
						Info("Race starts");
						RaceModel.GameStarted();
						send(new Ping());
//					send( new Throttle(1.0, 0) );
						break;

					case "carPositions":
						//Info("Car Positions received." );

						int gameTick = msg.gameTick == null ? 0 : (int)(Int64)msg.gameTick;
						GameTick = gameTick;

						if( !Simulating )
							RaceModel.CarPositions( msg.data as JArray, gameTick, SpeedModel );

						if( gameTick >= 0 && gameTick < SpeedModelData.Length )
							SpeedModelData[gameTick] = RaceModel.Me.Speed;

						if( gameTick == 0 )
						{
							Driver = (DriverBase)Activator.CreateInstance(DriverType, RaceModel, RaceModel.Me);
							//Driver = new DumbDriver(RaceModel, RaceModel.Me);
							//Driver = new HardcodedPieceSpeedsDriver(RaceModel, RaceModel.Me);
							//Driver = new HardcodedWaypointsDriver(RaceModel, RaceModel.Me);
							//Driver = new SimLookaheadDriver1(RaceModel, RaceModel.Me);
							//Driver = new SimLookaheadDriver2(RaceModel, RaceModel.Me));
							RaceModel.Me.Driver = Driver;
							Sim = new Car(RaceModel.Me);
						}
						else if( gameTick > 1 )
						{
							if( RaceModel.Me.Throttle != 1 )
								Sim.Accel = 0;
							//RaceModel.Simulate( Sim, SpeedModel, RaceModel.Me.Throttle);
						}
/*
					Info( string.Format("Sim speed error {0:f6} pos error {1:f} angle error {2:f6}",
											(RaceModel.Me.Speed - Sim.Speed),
											(RaceModel.Me.Dist - Sim.Dist),
											Utils.Degrees(RaceModel.Me.Angle - Sim.Angle)) );
*/
//						Debug.Assert( RaceModel.Me.Speed == Sim.Speed );

						var me = RaceModel.Me;

						if( gameTick == 5 && !Simulating )
						{
							// This is where we work out the acceleration constant and the throttle to speed ratio.
							// So far we've been accelerating with the pedal to the metal (throttle = 1.0)
							var data = string.Join(", ", SpeedModelData.Select(x => x.ToString()).ToArray());
							Info( "Speed Model Data: " + data );

							if( SpeedModelData[0] == 0 )
							{
								var zeroIdx = 0;
								while( zeroIdx + 1 < SpeedModelData.Length && SpeedModelData[zeroIdx + 1] == 0 )
									zeroIdx++;

								if( zeroIdx + 2 < SpeedModelData.Length )
								{
									SpeedModel = new SpeedModel(SpeedModelData[zeroIdx + 1], SpeedModelData[zeroIdx + 2], 1.0);
									Info( string.Format("Calculated speed model: AccelK={0:f3} ThrottleK={1:f3}",
											SpeedModel.AccelK, SpeedModel.ThrottleK) );
								}
							}
						}

						var useTurbo = false;
						{
							var runner = LaneRunners[me.EndLane];
							var targSpd = runner.CalcTargetSpeed(me.TrackPoint(), SpeedModel);

							me.TargetSpeed = targSpd;
							var thrtle = SpeedModel.ThrottleForSpeed(targSpd, me.Speed);

							if( thrtle >= 1.0 && runner.StraightDist(me.TrackPoint()) > 250 )
								useTurbo = true;
						}

						var control = Driver.ControlTick(gameTick, SpeedModel);
						//control.LaneRequest = DriverBase.LaneChange.None;

						var throttle = control.Throttle;

						//if( me.Lap < 2 )
						//	throttle = 0.5;

						throttle = Math.Max(0.0, Math.Min(1.0, throttle));

						// full throttle for the first few ticks... needed for the SpeedModel()
						// calculations at tick == 3.
						if( gameTick < 5 )
							throttle = 1.0;

						RaceModel.SetLastThrottle( throttle );
						me.Throttle = throttle;
						SendMsg req = new Throttle(throttle, gameTick);
//						Info( string.Format("thr {0:f}  ang {1:f}  simang {2:f}  spd {3:f}  dist {4:f}",
//											throttle, Utils.Degrees(me.Angle), Utils.Degrees(me.SimAngle),
//											me.Speed, me.Dist) );

						if( useTurbo && throttle == 1.0 && me.TurboTicks > 0 )
						{
							me.TurboTicks = 0;
							req = new Turbo(gameTick);
						}
						else if( me.Speed != 0 && control.LaneRequest != DriverBase.LaneChange.None )
						{
							var laneC = me.EndLane;
							var laneL = laneC - 1;
							var laneR = laneC + 1;

							switch( control.LaneRequest )
							{
								case DriverBase.LaneChange.Auto:
									{
										var distL = laneL >= 0 ? RaceModel.DistAfterNextSwitch(me, laneL) : 99999999;
										var distC = RaceModel.DistAfterNextSwitch(me, me.EndLane);
										var distR = laneR < RaceModel.Lanes.Count ? RaceModel.DistAfterNextSwitch(me, laneR) : 99999999;

										if( distL < distC && me.AskedLane != laneL )
										{
											RaceModel.Me.AskedLane = laneL;
											Sim.AskedLane = laneL;
											req = new SwitchLane("Left", gameTick);
										}
										else if( distR < distC && me.AskedLane != laneR )
										{
											RaceModel.Me.AskedLane = laneR;
											Sim.AskedLane = laneR;
											req = new SwitchLane("Right", gameTick);
										}
										break;
									}

								case DriverBase.LaneChange.Left: 
									if( me.AskedLane != laneL )
									{
										RaceModel.Me.AskedLane = laneL;
										Sim.AskedLane = laneL;
										req = new SwitchLane("Left", gameTick);
									}
									break;

								case DriverBase.LaneChange.Right: 
									if( me.AskedLane != laneR )
									{
										RaceModel.Me.AskedLane = laneR;
										Sim.AskedLane = laneR;
										req = new SwitchLane("Right", gameTick);
									}
									break;
							}
						}

//						if( gameTick == 0 )
//							req = new Ping();

						send( req );
						if( Simulating && gameTick != 0 )
						{
							RaceModel.Simulate( me, SpeedModel, throttle );
							RaceModel.InvokeModelUpdated( new EventArgs() );
							Thread.Sleep( 16 );
						}
						break;

					case "turboAvailable":
						Info( "Received msg: \"" + msg.msgType + "\": " + msg.data );
						var turbo = msg.data as JObject;
						//Info( "Turbo Available ticks=" + turbo["turboDurationTicks"] + " factor=" + turbo["turboFactor"] );
/*
						2014/04/20 21:24:16.23 - [Ratty4] Received msg: "turboAvailable": {
						  "turboDurationMilliseconds": 500.0,
						  "turboDurationTicks": 30,
						  "turboFactor": 3.0
						}
*/
						// TODO: change this back once fixed.
						// currently there seems to be a problem with this field missing when running on
						// the CI servers...
						RaceModel.Me.TurboTicks = 30;//(int)turbo["turboDurationTicks"];
						break;

					case "lapFinished":
						Info( "LapFinished: \"" + msg.msgType + "\": " + msg.data );
						RaceModel.LapFinisished( msg );
						break;

					case "crash":
						Info( "Crashed: \"" + msg.msgType + "\": " + msg.data );
						RaceModel.Crash( msg );
						if( (string)(msg.data as JObject)["name"] == RaceModel.Me.Info.Name )
						{
							Sim.Speed = 0;
							Sim.Accel = 0;
							Sim.Angle = 0;
							Sim.AngAccel = 0;
						}
						send(new Ping());
						break;

					case "spawn":
						Info( "Spawned: \"" + msg.msgType + "\": " + msg.data );
						RaceModel.Spawn( msg );
						send(new Ping());
						break;

					case "gameEnd":
						Info( "Race ended" );
						RaceModel.GameEnded();
						send(new Ping());
						break;

					default:
						Info( "Received msg: \"" + msg.msgType + "\": " + msg.data );
						send(new Ping());
						break;
				}
			}
		
			Info( "Run Loop has finished." );
		}

		private void send(SendMsg msg)
		{
			//Info( "Sent: " + msg.ToJson());
			Writer.WriteLine(msg.ToJson());
		}
	}

	public class MsgWrapper
	{
		public string	msgType;
		public Object	data;
		public Object	gameTick;

		public MsgWrapper(string msgType, Object data, Object gameTick)
		{
			this.msgType	= msgType;
			this.data		= data;
			this.gameTick   = gameTick;
		}
	}

	public class MsgWrapperNoTick
	{
		public string	msgType;
		public Object	data;

		public MsgWrapperNoTick(string msgType, Object data)
		{
			this.msgType	= msgType;
			this.data		= data;
		}
	}

	abstract class SendMsg
	{
		public string ToJson()
		{
			if( GameTick() >= 0 )
				return JsonConvert.SerializeObject(new MsgWrapper(MsgType(), MsgData(), GameTick()));
			else
				return JsonConvert.SerializeObject(new MsgWrapperNoTick(MsgType(), MsgData()));
		}

		protected virtual Object MsgData()
		{
			return this;
		}

		protected abstract string MsgType();
	
		protected virtual int GameTick()
		{
			return 0;
		}
	}

	class Join : SendMsg
	{
		public string name;
		public string key;

		public Join(string name, string key)
		{
			this.name = name;
			this.key = key;
		}

		protected override string MsgType()
		{
			return "join";
		}
	}

	class BotId
	{
		public string name;
		public string key;

		public BotId( string n, string k )
		{
			name = n;
			key  = k;
		}
	}

	class CreateRace : SendMsg
	{
		public BotId	botId;
		public string	trackName;
		public string	password;
		public int		carCount;

		public CreateRace( string track, string name, string key )
		{
			botId		= new BotId(name, key);
			trackName	= track;
			carCount	= 1;
		}

		public CreateRace( string track, string pw, string name, string key, int numBots )
		{
			botId		= new BotId(name, key);
			trackName	= track;
			password	= pw;
			carCount	= numBots;
		}

		public CreateRace( string track, string pw, string name, string key )
		{
			botId		= new BotId(name, key);
			trackName	= track;
			password	= pw;
			carCount	= 1;
		}

		protected override string MsgType()
		{
			return "createRace";
		}
	}

	class JoinRace : SendMsg
	{
		public BotId	botId;
		public string	trackName;
		public string	password;
		public int		carCount;

		public JoinRace( string track, string pw, string name, string key, int numBots )
		{
			botId		= new BotId(name, key);
			trackName	= track;
			carCount	= numBots;
			password	= pw;
		}

		protected override string MsgType()
		{
			return "joinRace";
		}
	}

	class Ping : SendMsg
	{
		protected override string MsgType()
		{
			return "ping";
		}
	}

	class Turbo : SendMsg
	{
//		public int _gameTick;

		public Turbo( int gameTick )
		{
			//_gameTick = gameTick;
		}

		protected override string MsgType()
		{
			return "turbo";
		}
/*
		protected override int GameTick()
		{
			return _gameTick;
		}*/

		protected override object MsgData()
		{
			return "!";
		}


		protected override int GameTick()
		{
			return -1;
		}
	}

	class Throttle: SendMsg
	{
		public double _value;
		public int _gameTick;

		public Throttle(double value, int gameTick)
		{
			_value = value;
			_gameTick = gameTick;
		}

		protected override Object MsgData()
		{
			return _value;
		}

		protected override string MsgType()
		{
			return "throttle";
		}

		protected override int GameTick()
		{
			return _gameTick;
		}
	}

	class SwitchLane : SendMsg
	{
		public string _direction;
		public int _gameTick;

		public SwitchLane(string direction, int gameTick)
		{
			_direction = direction;
			_gameTick = gameTick;
		}

		protected override Object MsgData()
		{
			return _direction;
		}

		protected override string MsgType()
		{
			return "switchLane";
		}

		protected override int GameTick()
		{
			return _gameTick;
		}
	}
}