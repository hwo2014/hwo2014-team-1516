﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mono
{
	/// <summary>
	/// stores information about a particular path around the track.
	/// answers questions like: 
	///		how long is it? 
	///		what lane should I be in?
	///		how fast should I be going???? -- not sure about this one.
	/// </summary>
	public class Path
	{
		public RaceModel RaceModel	{ get; private set; }

		private int[]		_startLanes;	// lane at start of piece.  for end lane, see start lane from next piece.
		private double[]	_distances;		// distances for each individual piece folling the path.
		private double[]	_totalDist;		// total dist from start for each individual piece folling the path.

		public double	TotalDist { get; private set; }

		public Path( RaceModel rm, int lane )
		{
			RaceModel = rm;

			_startLanes = new int[rm.Pieces.Count];
			for( int i = 0; i < _startLanes.Length; i++ )
				_startLanes[i] = lane;

			_distances = new double[_startLanes.Length];
			_totalDist = new double[_startLanes.Length];
			TotalDist = 0;
			for( int i = 0; i < _startLanes.Length; i++ )
			{
				_distances[i] = rm.Pieces[i].Dist(_startLanes[i],
												  _startLanes[next(i)]);
				_totalDist[i] = TotalDist;

				TotalDist += _distances[i];
			}
		}

		public int StartLane( int pieceIndex )
		{
			return _startLanes[pieceIndex];
		}

		public int EndLane( int pieceIndex )
		{
			return _startLanes[next(pieceIndex)];
		}

		public double	Dist( int pieceIndex )
		{
			return _distances[pieceIndex];
		}

		private int next( int index )
		{
			index += 1;
			if( index >= _distances.Length )
				index = 0;
			return index;
		}

		public double	DistBetweenPoints( TrackPoint tp1, TrackPoint tp2 )
		{
			if( tp1.Index < tp2.Index )
			{
				return	_totalDist[tp2.Index] - _totalDist[tp1.Index] +
						_distances[tp2.Index] * tp2.Fraction - _distances[tp1.Index] * tp1.Fraction;
			}
			else if( tp1.Index > tp2.Index )
			{
				return	TotalDist - _totalDist[tp1.Index] + _totalDist[tp2.Index] +
						_distances[tp2.Index] * tp2.Fraction - _distances[tp1.Index] * tp1.Fraction;
			}
			else if( tp1.Fraction < tp2.Fraction )
			{
				return _distances[tp1.Index] * (tp2.Fraction - tp1.Fraction);
			}
			else
			{
				return TotalDist - _distances[tp1.Index] * (tp1.Fraction - tp2.Fraction);
			}
/*
			double dist = _distances[tp1.Index] * (1 - tp1.Fraction);

			if( tp1.Index == tp2.Index && tp1.Fraction < tp2.Fraction )
			{
				dist -= _distances[tp2.Index] * (1 - tp2.Fraction);
			}
			else
			{
				int i = next(tp1.Index);
				while( i != tp2.Index )
				{
					dist += _distances[i];
					i = next(i);
				}

				dist += _distances[tp2.Index] * tp2.Fraction;
			}

			return dist;*/
		}

		private double calcTotalDist()
		{
			double totalDist = 0;
			for( int i = 0; i < _startLanes.Length; i++ )
				totalDist += _distances[i];
			return totalDist;
		}
	}
}
