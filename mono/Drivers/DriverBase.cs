﻿namespace mono.Drivers
{
	public abstract class DriverBase
	{
		public RaceModel	RaceModel;
		public Car			Car;

		public enum LaneChange
		{
			Auto,
			None,
			Left,
			Right,
		}

		public class CarControl
		{
			public double		Throttle;
			public LaneChange	LaneRequest;

			public CarControl( double throttle, LaneChange lane )
			{
				Throttle	= throttle;
				LaneRequest = lane;
			}
		}

		public DriverBase( RaceModel model, Car car )
		{
			RaceModel = model;
			Car = car;
		}

		public abstract CarControl ControlTick( int gameTick, SpeedModel sm );
	}
}
