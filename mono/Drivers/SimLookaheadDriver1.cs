﻿using System;

namespace mono.Drivers
{
	class SimLookaheadDriver1 : DriverBase
	{
		public SimLookaheadDriver1( RaceModel model, Car car )
		:	base(model, car)
		{
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			// run a simulation.  if it crashes, brake else floor it.
			double throttle = 1.0;
			bool crashed = true;
			var ticks = Car.Speed < 10 ? 45 : 60;
			var angLim = Car.Lap < 1 ? 50 : 55;
			//if( Car.Throttle != 1 )
			//	angLim = 50;
			while( throttle > 0 && crashed )
			{
				Car sim = new Car(Car);
				crashed = false;
				for( int i = 0; i < ticks; i++ )
				{
					RaceModel.Simulate(sim, sm, i < 1 ? throttle : 0.25);
//							RaceModel.Simulate(sim, SpeedModel, throttle);
					if( Utils.Degrees(Math.Abs(sim.Angle)) >= angLim )
					{
						crashed = true;
						throttle = 0;
						//throttle -= 0.1;
						//if( throttle == 1.0 )
						//	throttle = 0.5;
						//else
						//	throttle = 0.0;
						break;
					}
				}
			}

			return new CarControl(throttle, LaneChange.Auto);
		}
	}
}
