﻿namespace mono.Drivers
{
	class HardcodedWaypointsDriver : DriverBase
	{
		public HardcodedWaypointsDriver( RaceModel model, Car car )
		:	base(model, car)
		{
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			double throttle = 0.3;

			WayPoint[] waypoints = null;

			if( RaceModel.TrackId == "keimola" )
			{
				//acheived 7.3s, but using old inacurrate throttle control.
//				waypoints = new []{ new WayPoint(new TrackPoint( 5, 0.7),  5.1),
//									new WayPoint(new TrackPoint(15, 0.65), 5.0),
//									new WayPoint(new TrackPoint(20, 0.65), 5.1),
//									new WayPoint(new TrackPoint(27, 0.25), 5.9),
//									new WayPoint(new TrackPoint(30, 0),    6.1),
//									new WayPoint(new TrackPoint(32, 0.7),  5.2)
//								};

				// uses the new throttle control, but only gets to 7.317s.
				// 6.833 seconds with turbo... but turbo use is still sub-optimal.
				waypoints = new []{ new WayPoint(new TrackPoint( 5, 0.7),  5.6),
									new WayPoint(new TrackPoint(15, 0.7),  5.65),
									new WayPoint(new TrackPoint(20, 0.65), 5.45),
									new WayPoint(new TrackPoint(27, 0.25), 6.2),
									new WayPoint(new TrackPoint(30, 0),    6.3),
									new WayPoint(new TrackPoint(32, 0.75), 5.55)
								};

				// lane 0
//				waypoints = new []{ new WayPoint(new TrackPoint( 5, 0.47), 6.0),
//									new WayPoint(new TrackPoint(15, 0.68), 5.5),
//									new WayPoint(new TrackPoint(20, 0.7), 5.7),
//									new WayPoint(new TrackPoint(27, 0.25), 6.5),
//									new WayPoint(new TrackPoint(30, 0.3), 6.0),
//									new WayPoint(new TrackPoint(33, 0.0), 6.0)
//								};
			}

			if( RaceModel.TrackId == "germany" )
			{
				// 8.583s seconds with turbo... but turbo use is still sub-optimal.
				waypoints = new []{ new WayPoint(new TrackPoint( 4, 0.0), 4.7),
									new WayPoint(new TrackPoint( 7, 0.95), 4.35),	// perhaps less than next one..
									new WayPoint(new TrackPoint(11, 0.05), 4.35),
//									new WayPoint(new TrackPoint(13, 0.5), 3.5),
//									new WayPoint(new TrackPoint(16, 0.5), 3.5),
									new WayPoint(new TrackPoint(18, 0.25), 5.1),
									new WayPoint(new TrackPoint(25, 0.75), 4.0),	// top/rightmost 180
									new WayPoint(new TrackPoint(29, 0.0), 5.0),
//									new WayPoint(new TrackPoint(31, 0.5), 3.5),
//									new WayPoint(new TrackPoint(35, 0.5), 3.5),
									new WayPoint(new TrackPoint(38, 0.0), 4.8),
									new WayPoint(new TrackPoint(43, 0.5), 7.6),
									new WayPoint(new TrackPoint(47, 0.0), 5.4),
									new WayPoint(new TrackPoint(49, 0.0), 6.5),
									new WayPoint(new TrackPoint(52, 0.0), 5.8),
								};
			}

			if( RaceModel.TrackId == "usa" )
			{
				// 4.55s seconds with turbo... but turbo use is still sub-optimal.
				waypoints = new []{ new WayPoint(new TrackPoint( 3, 0.0), 8.95),
									new WayPoint(new TrackPoint( 4, 0.5), 9.15),
									new WayPoint(new TrackPoint( 9, 0.1), 8.97),
//									new WayPoint(new TrackPoint(10, 0.5), 9.25),
									new WayPoint(new TrackPoint(19, 0.0), 8.95),
									new WayPoint(new TrackPoint(20, 0.5), 9.15),
									new WayPoint(new TrackPoint(25, 0.1), 8.97),
//									new WayPoint(new TrackPoint(26, 0.5), 9.25),
								};
			}

			if( RaceModel.TrackId == "france" )
			{
				// 4.55s seconds with turbo... but turbo use is still sub-optimal.
				waypoints = new []{ //new WayPoint(new TrackPoint( 4, 0.0), 8.95),
									new WayPoint(new TrackPoint( 5, 0.7), 6.1),
									new WayPoint(new TrackPoint(10, 0.1), 8.3),
									new WayPoint(new TrackPoint(12, 0.5), 6.7),
									new WayPoint(new TrackPoint(15, 0.0), 4.3),
									new WayPoint(new TrackPoint(22, 0.25), 3.85),
									new WayPoint(new TrackPoint(27, 0.6), 5.7),
									new WayPoint(new TrackPoint(35, 0.7), 7.0),
								};
			}

			if( waypoints != null )
			{
				var tp = Car.TrackPoint();
				int wpIndex = 0;
				while( wpIndex < waypoints.Length && tp.IsAfter(waypoints[wpIndex].Tp) )
					wpIndex++;
				if( wpIndex >= waypoints.Length )
					wpIndex = 0;
				var wp = waypoints[wpIndex];
				var targSpd = sm.TargetSpeed(RaceModel.LanePaths[Car.EndLane].DistBetweenPoints(tp, wp.Tp), wp.Speed);
				if( Car.Speed > 10 )
					targSpd -= 1;

				Car.TargetSpeed = targSpd;
				throttle = sm.ThrottleForSpeed(targSpd, Car.Speed);
			}

			return new CarControl(throttle, LaneChange.Auto);
		}
	}
}
