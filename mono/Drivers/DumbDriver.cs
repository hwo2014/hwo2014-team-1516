﻿using System;

namespace mono.Drivers
{
	public class DumbDriver : DriverBase
	{
		public DumbDriver( RaceModel model, Car car )
		:	base(model, car)
		{
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			var turn = RaceModel.NextCurve(Car);
			var dist = RaceModel.DistToNextCurve(Car);
			var turnAng = Utils.Degrees(RaceModel.AngleOfNextCurve(Car));

//			var turnDist = RaceModel.DistLeftOfCurrentCurve(Car);

			var turnSpeed = Math.Sqrt(turn.LaneRadius(Car.EndLane) * 0.415);
//			var turnSpeed = Math.Sqrt(turn.LaneRadius(Car.EndLane) * 0.425);
//			var turnSpeed = Math.Sqrt(turn.LaneRadius(Car.EndLane) * 0.46);

			if( Math.Abs(turnAng) < 91 )
				turnSpeed += 0.3;

			var targSpeed = sm.TargetSpeed(dist, turnSpeed);
			var throttle = sm.ThrottleForSpeed(targSpeed, Car.Speed);

//			var attack = Math.Abs(Utils.Degrees(Car.Angle));

			return new CarControl(throttle, LaneChange.Auto);
//			return new CarControl(throttle, LaneChange.None);
		}
	}
}
