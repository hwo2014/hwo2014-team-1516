﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mono.Drivers
{
	class HardcodedPieceSpeedsDriver : DriverBase
	{
		public HardcodedPieceSpeedsDriver( RaceModel model, Car car )
		:	base(model, car)
		{
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			var turn = RaceModel.NextCurve(Car);
			var dist = RaceModel.DistToNextCurve(Car);
			var turnAng = Utils.Degrees(RaceModel.AngleOfNextCurve(Car));

//			var turnDist = RaceModel.DistLeftOfCurrentCurve(Car);

//			var turnSpeed = Math.Sqrt(turn.LaneRadius(me.EndLane) * 0.425);
			var turnSpeed = Math.Sqrt(turn.LaneRadius(Car.EndLane) * 0.46);

			double throttle = 0.3;

			if( dist > 100 )
				throttle = 1.0;
			else
			{
				var targSpd = turnSpeed;

				if( dist > 0 )
				{
					targSpd += 0.7;//0.5;
					if( Math.Abs(turnAng) < 91 )
						targSpd += 0.7;
				}

				if( RaceModel.TrackId == "keimola" )
				{
					switch( Car.Index )
					{
						case 4:
						case 5:		targSpd = 6.05;	break;
						case 6:		targSpd = 6.75;	break;
						case 7:		targSpd = 7.30;	break;

						case 14:	targSpd = 6.2;	break;
						case 15:	targSpd = 6.2;	break;
						case 16:	targSpd = 6.6;	break;
						case 17:	targSpd = 7.2;	break;

						case 19:
						case 20:	targSpd = 6.2;	break;
						case 21:	targSpd = 6.6;	break;
						case 22:	targSpd = 7.2;	break;

						case 26:	targSpd = 6.7;	break;
						case 27:	targSpd = 6.7;	break;

						case 29:
						case 30:	targSpd = 7.2;	break;

						case 31:	targSpd = 6.0;	break;
						case 32:	targSpd = 6.0;	break;
						case 33:	targSpd = 6.55;	break;
						case 34:	targSpd = 7.2;	break;
					}
				}

				throttle = sm.ThrottleForSpeed(targSpd, Car.Speed);
			}

			return new CarControl(throttle, LaneChange.Auto);
		}
	}
}
