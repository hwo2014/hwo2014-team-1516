﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace mono.Drivers
{
	enum Strategy
	{
		Starting,			// strategy into the 1st corner with cars ahead.
		FastLaps,			// just go around as fast as possible with no clever overtaking.
		OvertakeSlowerCar,	// try to change lanes to drive around a slower car in our lane.
		TurboBump,			// drive as fast as possible, and when safe, try to knock the car ahead off using turbo.
	}

	class SimLookaheadDriver2 : DriverBase
	{
		// manually tweak this so that it always manages to run around the track in spite of the
		// variation in physics constants.
		private double MaxSafeAngle1stLap = 30;
		private double MaxSafeAngle2ndLap = 45;
		private double MaxSafeAngleOngoing = 55;//58;

		// strategy
		Strategy	_strategy = Strategy.Starting;

		// info about nearby cars.
		readonly List<Car>	_toLeft = new List<Car>();
		readonly List<Car>	_toRight = new List<Car>();
		readonly List<Car>	_collidingAhead = new List<Car>();
		readonly List<Car>	_collidingBehind = new List<Car>();
		readonly List<Car>	_justAhead = new List<Car>();
		readonly List<Car>	_justAheadToLeft = new List<Car>();
		readonly List<Car>	_justAheadToRight = new List<Car>();
		Car					_nearestCarAhead;
		double				_nearestCarAheadDist;

		int[]				_lastPiece;
		int[,]				_pieceTicks;		// keep track of the tick when each char entered the piece.
		int[]				_ticksAhead;		// how many ticks the other car is ahead (+ve if ahead.)
		int[]				_ticksAheadDelta;	// change in last number of ticks ahead.

		public SimLookaheadDriver2( RaceModel model, Car car )
		:	base(model, car)
		{
			_lastPiece			= new int[model.Cars.Count];
			for( int i = 0; i < _lastPiece.Length; i++ )
				_lastPiece[i] = -1;
			_pieceTicks			= new int[model.Cars.Count, model.Pieces.Count];
			_ticksAhead		= new int[model.Cars.Count];
			_ticksAheadDelta	= new int[model.Cars.Count];
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			ProcessNearbyCars( gameTick );

			var newStrategy = ChooseStrategy(gameTick);
			if( newStrategy != _strategy )
			{
				Log.Info( "[" + gameTick + "] Strategy changed to: " + newStrategy );
				_strategy = newStrategy;
			}

			switch( _strategy )
			{
				case Strategy.FastLaps:
				default:
					return FastStrategy(gameTick, sm);

				case Strategy.Starting:
				{
					// choose least busy lane ahead?
					if( Car.StartLane != Car.EndLane )
						// currently switching... nothing to do.
						return FastStrategy(gameTick, sm);

					int leftCount = Car.EndLane > 0 ? _justAheadToLeft.Count : 9999;
					int sameCount = _justAhead.Count;
					int rightCount = Car.EndLane + 1 < RaceModel.Lanes.Count ? _justAheadToRight.Count : 9999;

					var laneChange = LaneChange.None;
					if( leftCount < sameCount && leftCount < rightCount )
						laneChange = LaneChange.Left;
					if( rightCount < sameCount && rightCount < leftCount )
						laneChange = LaneChange.Right;
					return new CarControl(CalcThrottle(Car, sm), laneChange);
				}

				case Strategy.OvertakeSlowerCar:
				{
					Debug.Assert( _nearestCarAhead != null );
					Debug.Assert( Car.EndLane == _nearestCarAhead.EndLane );

					if( CarBesideUs() )
						return FastStrategy(gameTick, sm);

					var laneChange = LaneChange.Auto;

					// wait until the car ahead is on the next switch, and chosse to go
					// the other way.
					if( RaceModel.Pieces[_nearestCarAhead.Index].Switch )
					{
						if( Car.EndLane > 0 )
							laneChange = LaneChange.Left;
						else if( Car.EndLane + 1 < RaceModel.Lanes.Count )
							laneChange = LaneChange.Right;
					}
					return new CarControl(CalcThrottle(Car, sm), laneChange);
				}
			}
		}

		private Strategy ChooseStrategy( int gameTick )
		{
			var aheadCount = _justAhead.Count + _justAheadToLeft.Count + _justAheadToRight.Count;
			if( aheadCount > 1 && Car.Lap <= 1 && Car.Index <= RaceModel.Pieces.Count / 4 )
			{
				return Strategy.Starting;
			}

			if( _nearestCarAhead != null )
			{
				if( CatchingCarAhead(_nearestCarAhead) ||
					CarHasSlowerLapTimes(_nearestCarAhead) )
				{
					return Strategy.OvertakeSlowerCar;
				}
				else
				{
					//return Strategy.TurboBump;
					return Strategy.FastLaps;
				}
			}

			return Strategy.FastLaps;
		}

		private CarControl FastStrategy( int gameTick, SpeedModel sm )
		{
			return new CarControl(CalcThrottle(Car, sm), ChooseLane());
		}

		private bool CatchingCarAhead( Car otherCar )
		{
			var id = otherCar.Info.Id;
			if( _ticksAhead[id] < 0 )
			{
				// don't know.
				return false;
			}

			return _ticksAheadDelta[id] < 0;
		}
		private bool CarHasSlowerLapTimes( Car otherCar )
		{
			if( Car.BestLapTicks == 0 || otherCar.BestLapTicks == 0 )
				// don't have any lap times to compare.
				return false;

			return Car.BestLapTicks < otherCar.BestLapTicks;
		}

		private void ProcessNearbyCars( int gameTick )
		{
			_toLeft.Clear();
			_toRight.Clear();
			_collidingAhead.Clear();
			_collidingBehind.Clear();
			_justAhead.Clear();
			_justAheadToLeft.Clear();
			_justAheadToRight.Clear();

			_nearestCarAhead = null;
			_nearestCarAheadDist = 0;

			var myId   = Car.Info.Id;
			var myLane = Car.EndLane;

			var myLastPiece = _lastPiece[myId];

			foreach( var otherCar in RaceModel.Cars )
			{
				var id = otherCar.Info.Id;
				if( otherCar.Index != _lastPiece[id] )
				{
					// other car has now moved into a new piece.
					_lastPiece[id] = otherCar.Index;
					_pieceTicks[id, otherCar.Index] = gameTick;
				}

				if( otherCar.IsSameCar(Car) )
					continue;

				double distAhead = DistToCar(otherCar);
				if( distAhead > -Car.Info.Length && distAhead < Car.Info.Length )
				{
					// close, to side or same lane?
					if( myLane == otherCar.EndLane )
					{
						// colliding with other car.
						if( distAhead > 0 )
							_collidingAhead.Add( otherCar );
						else
							_collidingBehind.Add( otherCar );
					}
					else
					{
						if( myLane > otherCar.EndLane )
							_toLeft.Add( otherCar );
						else
							_toRight.Add( otherCar );
					}
				}
				else if( distAhead > 0 && distAhead < Car.Info.Length * 3 )
				{
					if( myLane == otherCar.EndLane )
					{
						_justAhead.Add( otherCar );
						if( _nearestCarAhead == null ||
							_nearestCarAheadDist > distAhead )
						{
							_nearestCarAhead = otherCar;
							_nearestCarAheadDist = distAhead;
						}
					}
					else
					{
						if( myLane > otherCar.EndLane )
							_justAheadToLeft.Add( otherCar );
						else
							_justAheadToRight.Add( otherCar );
					}
				}
			}

			if( myLastPiece != Car.Index )
			{
				// our car has just entered a new piece.
				// update how far we think we are behind the other cars.

				foreach( var otherCar in RaceModel.Cars )
				{
					var id = otherCar.Info.Id;
					if( id == myId )
						continue;

					var ticks = _pieceTicks[id, Car.Index];
					if( ticks < 0 )
					{
						// other car hasn't been here yet.
						_ticksAhead[id] = -1;
						_ticksAheadDelta[id] = -1;
					}
					else
					{
						if( _ticksAhead[id] < 0 )
						{
							// other car has been here, but we don't have enough info for a delta.
							_ticksAhead[id] = gameTick - ticks;
							_ticksAheadDelta[id] = 0;//??
						}
						else
						{
							var ticksAhead = gameTick - ticks;
							_ticksAheadDelta[id] = _ticksAhead[id] - ticksAhead;
							_ticksAhead[id] = ticksAhead;
						}
					}
				}
			}
		}

		private double ChooseThrottle( SpeedModel sm )
		{
			if( SafeToNudgeCarAhead(sm) )
			{				
				Log.Info( Car.Info.Name + "(" + Car.Info.Color + "): Bump Accel" );
				return 1.0;
			}

			return CalcThrottle(Car, sm);
		}

		private double CalcThrottle( Car me, SpeedModel sm )
		{
			//var maxAng = RaceModel.Qualifying && me.Lap < 2 ? MaxSafeAngleStart : MaxSafeAngle;
			var maxAng = MaxSafeAngleOngoing;
			if( RaceModel.RaceNumber <= 2 && me.Lap < 2 )
				maxAng = me.Lap < 1 ? MaxSafeAngle1stLap : MaxSafeAngle2ndLap;

			var tMin = 0.0;
			var tMax = 1.0;
			var ticks = me.Speed < 10 ? 40 : 60;
			while( tMin + 0.001 < tMax )
			{
				var sim = new Car(me);

				var tMid = (tMin + tMax) * 0.5;
						
				var crashed = false;
				for( var i = 0; i < ticks; i++ )
				{
					if( RaceModel.DriftModel.GotData(sim.Index, sim.Radius(), sim.Speed) )
						maxAng = MaxSafeAngleOngoing;
					else
						maxAng = MaxSafeAngle1stLap;

					RaceModel.Simulate(sim, sm, i < 1 ? tMid : 0.15);
//					RaceModel.Simulate(sim, SpeedModel, tMid);
					if( Utils.Degrees(Math.Abs(sim.Angle)) > maxAng )
//					if( Utils.Degrees(Math.Abs(sim.Angle)) > 58 )
					{
						crashed = true;
						break;
					}
				}

				if( crashed )
					tMax = tMid;
				else
					tMin = tMid;
			}

			var throttle = tMax == 1.0 ? 1.0 : tMin;
			return throttle;
		}

        // this is a crude calculation of the distance of a car ahead.
		private double DistToNearestCarAhead( bool includeToSide, bool sameLaneOnly )
		{
            var carAhead = NearestCarAhead(includeToSide, sameLaneOnly);
			if( carAhead == null )
				return RaceModel.TotalDist / 2;

			return DistToCar(carAhead);
        }


		private double DistToCar( Car otherCar )
		{
            var myDist = RaceModel.Pieces[Car.Index].TotalDist + Car.Dist;
            var otherDist = RaceModel.Pieces[otherCar.Index].TotalDist + otherCar.Dist;

			var dist = otherDist - myDist;
			var halfTotalDist = RaceModel.TotalDist / 2;

			while( dist > halfTotalDist )
				dist -= RaceModel.TotalDist;
			while( dist < -halfTotalDist )
				dist += RaceModel.TotalDist;
			return dist;
		}

        // this is a crude calculation of the distance of a car ahead.
		private Car NearestCarAhead( bool includeToSide, bool sameLaneOnly )
		{
            var myDist = RaceModel.Pieces[Car.Index].TotalDist + Car.Dist;

			var bestAhead = RaceModel.TotalDist / 2;
			Car carAhead  = null;

			var minDistAhead = includeToSide ? -Car.Info.Length : 0;

            foreach( var otherCar in RaceModel.Cars )
            {
                if( Car.IsSameCar(otherCar) )
                    continue;

				if( sameLaneOnly && (otherCar.EndLane != Car.EndLane || otherCar.StartLane != Car.EndLane) )
					continue;

                var otherDist = RaceModel.Pieces[otherCar.Index].TotalDist + otherCar.Dist;
				var ahead = otherDist - myDist;
				if( ahead < bestAhead && ahead > minDistAhead )
				{
					bestAhead = ahead;
					carAhead  = otherCar;
				}
            }

            return carAhead;
        }

		private bool SafeToNudgeCarAhead( SpeedModel sm )
		{
			var carAhead = NearestCarAhead(false, true);
			if( carAhead == null )
				return false;

			var aheadDist = DistToCar(carAhead);
			if( aheadDist > Car.Info.Length * 2 )
				return false;

			if( carAhead.Accel > 0 )
				return false;

			carAhead = new Car(carAhead);
			var me = new Car(Car);

			Log.Info( Car.Info.Name + "(" + Car.Info.Color + "): Got Car Ahead " + aheadDist );

			var ticks = 30;
			for( int i = 0; i < ticks; i++ )
			{
				RaceModel.Simulate( carAhead, sm, CalcThrottle(carAhead, sm) );
				RaceModel.Simulate( me, sm, 1.0 );

				if( Math.Abs(me.Angle) > MaxSafeAngleOngoing )
					return false;

				if( DistToCar(carAhead) < Car.Info.Length )
					return true;
			}

			return false;
		}

        private bool CarBesideUs()
        {
			if( _toLeft.Count == 0 && _toRight.Count == 0 )
				return false;

			// only count cars that are not too far behind.
            foreach( var otherCar in _toLeft )
			{
				var distAhead = DistToCar(otherCar);
				if( distAhead > -Car.Info.Length / 2 )
					return true;
			}

            foreach( var otherCar in _toRight )
			{
				var distAhead = DistToCar(otherCar);
				if( distAhead > -Car.Info.Length / 2 )
					return true;
			}

			return false;
/*
            foreach (var listCar in RaceModel.Cars)
            {
                if( Car.IsSameCar(listCar) )
                    continue;

                // this comparison is of course very simple.  A more advanced 
                // version would be adjusting lengths for drift angle, as well
                // as calculating speed differences.
                double myDist = RaceModel.Pieces[Car.Index].TotalDist + Car.Dist;
                double otherDist = RaceModel.Pieces[listCar.Index].TotalDist + listCar.Dist;

                if (otherDist > myDist - Car.Info.Length &&
                    otherDist < myDist + listCar.Info.Length)
                {
                    return true;
                }
            }

            return false;*/
        }

        private LaneChange ChooseLane()
        {
            if (CarBesideUs() == true)
            {
                return LaneChange.None;
            }

            return LaneChange.Auto;
        }
	}
}
