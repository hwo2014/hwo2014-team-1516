﻿using System;
using System.Diagnostics;

namespace mono.Drivers
{
	class SimLookaheadDriver3 : DriverBase
	{
		// manually tweak this so that it always manages to run around the track in spite of the
		// variation in physics constants.
		private double MaxSafeAngle1stLap = 30;
		private double MaxSafeAngle2ndLap = 45;
		private double MaxSafeAngleOngoing = 55;//58;

		enum TurnMode	// TODO: not yet used
		{
			Clear,		// this means that the robot sees no need to brake at all, %100 pedal to the metal.
			PreEntry,	// was clear, now has to brake at some point soon.
			Entry,		// is now braking for a bend.
			Apex,		// hit apex.  how holding a apex speed until starting exit
		}

		TurnMode	_turnMode = TurnMode.Clear;
		double		_apexSpeed = 0;

		public SimLookaheadDriver3( RaceModel model, Car car )
		:	base(model, car)
		{
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			var ticks = Car.Speed < 10 ? 30 : 60;

			var throttle = 1.0;

			var maxAng = MaxSafeAngleOngoing;
			if( RaceModel.RaceNumber <= 2 && Car.Lap < 2 )
				maxAng = Car.Lap < 1 ? MaxSafeAngle1stLap : MaxSafeAngle2ndLap;

			switch( _turnMode )
			{
				case TurnMode.Clear:
				{
					// check if still OK to floor it.
					int crashTicks = SimTicksUntilCrashes(ticks, sm, 1.0, 1.0, maxAng);
					if( crashTicks < ticks )
					{
						Debug.Assert( crashTicks > 0 );
						_turnMode = TurnMode.PreEntry;
					}
					break;
				}

				case TurnMode.PreEntry:
				{
					int crashTicks = SimTicksUntilCrashes(ticks, sm, 1.0, 0.25, maxAng);
					if( crashTicks < ticks  )
					{
						_turnMode = TurnMode.Entry;
						throttle = 0.0;
					}
					break;
				}

				case TurnMode.Entry:
				{
					throttle = 0.0;

					// wait until angle speed is in opposite direction to angle.
					if( Car.AngSpeed != 0 && Car.Angle != 0 &&
						Car.AngSpeed * Car.Angle < 0 &&
						Car.Radius() != 0 )
					{
						_turnMode = TurnMode.Apex;
						_apexSpeed = Car.Speed;
					}
					break;
				}
					
				case TurnMode.Apex:
				{
					//throttle = 0;
					throttle = sm.ThrottleForSpeed(Car.Speed, _apexSpeed);

					// wait until it's possible to floor it to exit.
					int crashTicks = SimTicksUntilCrashes(ticks, sm, 1.0, 1.0, maxAng);
					if( crashTicks == ticks )
					{
						_turnMode = TurnMode.Clear;
						throttle = 1.0;
					}
					break;
				}
			}
/*
			var tMin = 0.0;
			var tMax = 1.0;
			while( tMin + 0.001 < tMax )
			{
				var tMid = (tMin + tMax) * 0.5;
						
				if( SimTicksUntilCrashes(ticks, sm, tMid, 0.25, maxAng) > 1 )
					tMax = tMid;
				else
					tMin = tMid;
			}
			
			throttle = tMax == 1.0 ? 1.0 : tMin;
*/
			return new CarControl(throttle, LaneChange.Auto);
		}

		private int SimTicksUntilCrashes(
			int ticks, SpeedModel sm,
			double initialThrottle, double continuingThrottle,
			double driftLimit )
		{
			var sim = new Car(Car);
			for( var i = 0; i < ticks; i++ )
			{
				RaceModel.Simulate(sim, sm, i < 1 ? initialThrottle : continuingThrottle);
				if( Utils.Degrees(Math.Abs(sim.Angle)) > driftLimit )
					return i;
			}
			return ticks;
		}
	}
}
