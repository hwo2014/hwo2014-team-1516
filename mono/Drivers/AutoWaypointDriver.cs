﻿namespace mono.Drivers
{
	class AutoWaypointDriver : DriverBase
	{
		private DriverBase		_fallbackDriver;

		private PathRunner[]	_laneRunners { get; set; }

		public AutoWaypointDriver( RaceModel model, Car car )
		:	base(model, car)
		{
			_fallbackDriver = new DumbDriver(model, car);

			_laneRunners = new PathRunner[model.Lanes.Count];
			for( int i = 0; i < _laneRunners.Length; i++ )
				_laneRunners[i] = new PathRunner(model.LanePaths[i]);
		}

		public override CarControl ControlTick( int gameTick, SpeedModel sm )
		{
			if( gameTick == 5 )
			{
				foreach( var r  in _laneRunners )
				{
					r.Optimise( sm );
					//r.Optimise( SpeedModel );
					//r.Optimise( SpeedModel );
				}
			}
			
			var runner = _laneRunners[Car.EndLane];
			var targSpd = runner.CalcTargetSpeed(Car.TrackPoint(), sm);

			Car.TargetSpeed = targSpd;
			var throttle = sm.ThrottleForSpeed(targSpd, Car.Speed);

			return new CarControl(throttle, LaneChange.Auto);
		}
	}
}
