﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace mono
{
	class RevEngDriftModel : IDriftModel
	{
		// for each piece,
		//	for each radius,
		//	 for each speed category
		//	  CoefficientsItem

		readonly List<PieceItem>	_items = new List<PieceItem>();

		public void NewData( int gameTick, Car car, int newIndex, double newRotAcc )
		{
			if( newIndex == 53 )
			{
				newRotAcc *= 1;
			}

			if( car.Index		!= newIndex		||
				car.Angle		== 0			||
				car.AngSpeed	== 0			||
				car.StartLane   != car.EndLane	||
				!car.IsRunning					||
				!car.SpeedValid					||
				car.Speed		<  3 )
			{
				return;
			}

			while( _items.Count <= newIndex )
			{
				_items.Add( new PieceItem() );
			}

			var pieceItem = _items[newIndex];
			pieceItem.NewData(car, newRotAcc );
		}

		public bool Apply(
			int piece, double radius, double speed,
			ref double angle, ref double rotSpd, ref double rotAcc )
		{
			if( piece >= _items.Count )
				return false;

			return _items[piece].Apply(radius, speed, ref angle, ref rotSpd, ref rotAcc);
		}

		public string GetUiString()
		{
			return string.Empty;
		}

		public void DumpCoefficients()
		{
			for( var i = 0; i < _items.Count; i++ )
			{
				_items[i].Dump( i );
			}
		}

		public bool GotData( int index, double radius, double speed )
		{
			if( index < 0 || index >= _items.Count )
				return false;

			return _items[index].GotData(radius, speed);
			
		}

		class PieceItem
		{
			readonly Dictionary<int, RadiusItem>	_items = new Dictionary<int, RadiusItem>();

			public void NewData(Car car, double newRotAcc)
			{
				var radius = (int)Math.Round(car.Radius());
				if( !_items.ContainsKey(radius) )
				{
					_items.Add( radius, new RadiusItem() );
				}

				_items[radius].NewData(car, newRotAcc );
			}

			public bool Apply(double radius, double speed, ref double angle, ref double rotSpd, ref double rotAcc)
			{
				var intRadius = (int)Math.Round(radius);
				if( !_items.ContainsKey(intRadius) )
					return false;

				return _items[intRadius].Apply(radius, speed, ref angle, ref rotSpd, ref rotAcc );
			}

			public void Dump( int index )
			{
				foreach( var item in _items )
				{
					item.Value.Dump( index, item.Key );
				}
			}

			public bool GotData( double radius, double speed )
			{
				var intRadius = (int)Math.Round(radius);
				if( !_items.ContainsKey(intRadius) )
					return false;

				return _items[intRadius].GotData(radius, speed);
			}
		}

		class RadiusItem
		{
			readonly CoefficientsItem	_slowItem = new CoefficientsItem();
			readonly CoefficientsItem	_fastItem = new CoefficientsItem();

			// impirically measured (TODO: may change on different tracks.)
			const double LatAccLimit = 0.32;

			public void NewData(Car car, double newRotAcc)
			{
				var latAcc = Math.Abs(car.Speed * car.Speed * car.Curvature());

				if( latAcc < LatAccLimit * 0.95 )
				{
					_slowItem.NewData( newRotAcc, new []{car.AngSpeed, car.Angle * car.Speed} );
				}
				else if( latAcc > LatAccLimit * 1.05 )
				{
					_fastItem.NewData( newRotAcc, new []{car.AngSpeed, car.Angle * car.Speed, car.Speed, car.Speed * car.Speed} );
				}
			}

			public bool Apply(double radius, double speed, ref double angle, ref double rotSpd, ref double rotAcc)
			{
				var latAcc = radius == 0 ? 0 : speed * speed / Math.Abs(radius);

				double newRotAcc;
				if( latAcc < LatAccLimit )
				{
					newRotAcc = _slowItem.Evaluate(new []{rotSpd, angle * speed});

					// if we don't have data for the slow calc, but we do have the data for fast one, then
					// we can use the fast one instead by just missing out the trailing speed and speed-squared variables.
					if( Double.IsNaN(newRotAcc) )
						newRotAcc = _fastItem.Evaluate(new []{rotSpd, angle * speed});
				}
				else
					newRotAcc = _fastItem.Evaluate(new []{rotSpd, angle * speed, speed, speed * speed});

				if( Double.IsNaN(newRotAcc) )
				{
					// this is expected until we've accumulated enough data to work out some coefficients.
					return false;
				}

				var newRotSpd = rotSpd + newRotAcc;
				var newAngle  = angle  + rotSpd;
			
				angle  = newAngle;
				rotSpd = newRotSpd;
				rotAcc = newRotAcc;

				return true;
			}

			public void Dump( int index, int radius )
			{
				if( _slowItem != null && _slowItem.HasCoefficients )
					Log.Info( "[" + index + "] r=" + radius + "  slow: " + _slowItem );

				if( _fastItem != null && _fastItem.HasCoefficients )
					Log.Info( "[" + index + "] r=" + radius + "  fast: " + _fastItem );
			}

			public bool GotData( double radius, double speed )
			{
				var latAcc = radius == 0 ? 0 : speed * speed / Math.Abs(radius);
				if( latAcc < LatAccLimit )
					return _slowItem != null;
				else
					return _fastItem != null;
			}
		}

		class CoefficientsItem
		{
			double[]		_coefficients;
			List<double[]>	_coefficientPool = new List<double[]>();

			List<double>	_yValues = new List<double>();
			List<double[]>	_xValues = new List<double[]>();

			public bool HasCoefficients { get { return _coefficients != null;  } }

			public void NewData( double yValue, double[] xValues )
			{
				//if( _coefficientPool.Count >= 3 )
				//	return;

				if( _xValues.Count != 0 )
				{
					var last = _xValues.Last();
					for( int i = 0; i < xValues.Length; i++ )
						if( Math.Abs(last[i] - xValues[i]) < 1E-7 )
							return;
				}

				_yValues.Add( yValue );
				_xValues.Add( xValues );

				if( _yValues.Count == xValues.Length )
				{
					var m = new Matrix(_yValues.Count);
					var v = new double[_yValues.Count];
					for( int i = 0; i < _yValues.Count; i++ )
					{
						//Log.Info( Utils.ToString(_xValues[i]) );

						for( var j = 0; j < _xValues[i].Length; j++ )
						{
							m.Set( i, j, _xValues[i][j] );
						}

						v[i] = _yValues[i];
					}

					try
					{
						var coeffs = m.GaussJordan(v);

						// coarse filter for the odd strange result (like large positive values.)
						// (the angular speed coefficient is expected to be approx. -0.1.)
						if( -0.3 < coeffs[0] && coeffs[0] < -0.03 )
						{
							_coefficientPool.Add( coeffs );
							ChooseCoeffsFromPool();
						}
					}
					catch( Exception ex )
					{
						Log.Info( ex.Message + " :-" );
						Log.Info( m.ToString() );
					}

					_yValues.Clear();
					_xValues.Clear();
				} 
			}

			public double Evaluate( double[] xValues )
			{
				if( _coefficients == null )
					return Double.NaN;

				return xValues.Select((t, i) => t * _coefficients[i]).Sum();
			}

			private void ChooseCoeffsFromPool()
			{
				if( _coefficientPool.Count == 0 )
					return;

				// choose closest set of coeffs to the average...

				var len = _coefficientPool[0].Length;

				var avg = new double[len];
				foreach( var coeffs in _coefficientPool )
				{
					for( var i = 0; i < len; i++ )
					{
						avg[i] += coeffs[i];
					}
				}
				for( var i = 0; i < len; i++ )
				{
					avg[i] /= _coefficientPool.Count;
				}

				double[] best = null;
				var bestDist = Double.MaxValue;
				double[] worst = null;
				var worstDist = -1.0;
				foreach( var coeffs in _coefficientPool )
				{
					var dist = 0.0;
					for( var i = 0; i < len; i++ )
					{
						var diff = avg[i] - coeffs[i];
						dist += diff * diff;
					}

					if( bestDist > dist )
					{
						bestDist = dist;
						best = coeffs;
					}

					if( worstDist < dist )
					{
						worstDist = dist;
						worst = coeffs;
					}
				}

				if( _coefficientPool.Count > 10 && best != worst )
					_coefficientPool.Remove( worst );

				_coefficients = best;
			}

			public override string ToString()
			{
				if( _coefficients == null )
					return "null";

				return Utils.ToString(_coefficients);
			}
		}
	}
}