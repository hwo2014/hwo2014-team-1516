﻿namespace mono.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.StartRaceButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.BotKeyTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.PortUpDown = new System.Windows.Forms.NumericUpDown();
			this.StartReplayButton = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.ReplayFilenameTextBox = new System.Windows.Forms.TextBox();
			this.TrackNameComboBox = new System.Windows.Forms.ComboBox();
			this.TrackNameLabel = new System.Windows.Forms.Label();
			this.TotalBotsLabel = new System.Windows.Forms.Label();
			this.TotalBotsUpDown = new System.Windows.Forms.NumericUpDown();
			this.HostComboBox = new System.Windows.Forms.ComboBox();
			this.SimpleJoinRadioButton = new System.Windows.Forms.RadioButton();
			this.label7 = new System.Windows.Forms.Label();
			this.BotNameTextBox = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.CreateRaceRadioButton = new System.Windows.Forms.RadioButton();
			this.JoinRaceRadioButton = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.SameAsTotalCheckBox = new System.Windows.Forms.CheckBox();
			this.PrivateRaceCheckBox = new System.Windows.Forms.CheckBox();
			this.OurBotsLabel = new System.Windows.Forms.Label();
			this.OurBotsUpDown = new System.Windows.Forms.NumericUpDown();
			this.BotPositionUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.OtherDriverTypeComboBox = new System.Windows.Forms.ComboBox();
			this.DriverTypeComboBox = new System.Windows.Forms.ComboBox();
			this.PasswordTextBox = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.PortUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalBotsUpDown)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.OurBotsUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BotPositionUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// StartRaceButton
			// 
			this.StartRaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.StartRaceButton.Location = new System.Drawing.Point(307, 179);
			this.StartRaceButton.Name = "StartRaceButton";
			this.StartRaceButton.Size = new System.Drawing.Size(73, 23);
			this.StartRaceButton.TabIndex = 11;
			this.StartRaceButton.Text = "&Start Race";
			this.StartRaceButton.UseVisualStyleBackColor = true;
			this.StartRaceButton.Click += new System.EventHandler(this.StartRaceButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "&Server:";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(221, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Bot &Key:";
			// 
			// BotKeyTextBox
			// 
			this.BotKeyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BotKeyTextBox.Location = new System.Drawing.Point(274, 39);
			this.BotKeyTextBox.Name = "BotKeyTextBox";
			this.BotKeyTextBox.Size = new System.Drawing.Size(124, 20);
			this.BotKeyTextBox.TabIndex = 7;
			this.BotKeyTextBox.Text = "9OtAawpeKGp74Q";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(305, 15);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(29, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "&Port:";
			// 
			// PortUpDown
			// 
			this.PortUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.PortUpDown.Location = new System.Drawing.Point(341, 12);
			this.PortUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.PortUpDown.Name = "PortUpDown";
			this.PortUpDown.Size = new System.Drawing.Size(57, 20);
			this.PortUpDown.TabIndex = 3;
			this.PortUpDown.Value = new decimal(new int[] {
            8091,
            0,
            0,
            0});
			// 
			// StartReplayButton
			// 
			this.StartReplayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.StartReplayButton.Location = new System.Drawing.Point(325, 396);
			this.StartReplayButton.Name = "StartReplayButton";
			this.StartReplayButton.Size = new System.Drawing.Size(73, 23);
			this.StartReplayButton.TabIndex = 12;
			this.StartReplayButton.Text = "Start &Replay";
			this.StartReplayButton.UseVisualStyleBackColor = true;
			this.StartReplayButton.Click += new System.EventHandler(this.ReplayRaceButton_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 401);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(88, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Replay &Filename:";
			// 
			// ReplayFilenameTextBox
			// 
			this.ReplayFilenameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ReplayFilenameTextBox.Location = new System.Drawing.Point(106, 398);
			this.ReplayFilenameTextBox.Name = "ReplayFilenameTextBox";
			this.ReplayFilenameTextBox.Size = new System.Drawing.Size(213, 20);
			this.ReplayFilenameTextBox.TabIndex = 11;
			this.ReplayFilenameTextBox.Text = "..\\..\\Race-Example-Json-Log.txt";
			// 
			// TrackNameComboBox
			// 
			this.TrackNameComboBox.FormattingEnabled = true;
			this.TrackNameComboBox.Items.AddRange(new object[] {
            "keimola (Finland)",
            "germany (Germany)",
            "usa (Easter Egg)",
            "france (France)",
            "elaeintarha (Eläintarha)",
            "imola (Italy)",
            "england (England)",
            "suzuka (Japan)"});
			this.TrackNameComboBox.Location = new System.Drawing.Point(81, 19);
			this.TrackNameComboBox.Name = "TrackNameComboBox";
			this.TrackNameComboBox.Size = new System.Drawing.Size(171, 21);
			this.TrackNameComboBox.TabIndex = 1;
			// 
			// TrackNameLabel
			// 
			this.TrackNameLabel.AutoSize = true;
			this.TrackNameLabel.Location = new System.Drawing.Point(6, 22);
			this.TrackNameLabel.Name = "TrackNameLabel";
			this.TrackNameLabel.Size = new System.Drawing.Size(69, 13);
			this.TrackNameLabel.TabIndex = 0;
			this.TrackNameLabel.Text = "Track Name:";
			// 
			// TotalBotsLabel
			// 
			this.TotalBotsLabel.AutoSize = true;
			this.TotalBotsLabel.Location = new System.Drawing.Point(6, 78);
			this.TotalBotsLabel.Name = "TotalBotsLabel";
			this.TotalBotsLabel.Size = new System.Drawing.Size(142, 13);
			this.TotalBotsLabel.TabIndex = 4;
			this.TotalBotsLabel.Text = "&Total number of bots in race:";
			// 
			// TotalBotsUpDown
			// 
			this.TotalBotsUpDown.Location = new System.Drawing.Point(154, 76);
			this.TotalBotsUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.TotalBotsUpDown.Name = "TotalBotsUpDown";
			this.TotalBotsUpDown.Size = new System.Drawing.Size(52, 20);
			this.TotalBotsUpDown.TabIndex = 5;
			this.TotalBotsUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.TotalBotsUpDown.ValueChanged += new System.EventHandler(this.TotalBotsUpDown_ValueChanged);
			// 
			// HostComboBox
			// 
			this.HostComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.HostComboBox.FormattingEnabled = true;
			this.HostComboBox.Items.AddRange(new object[] {
            "testserver.helloworldopen.com",
            "hakkinen.helloworldopen.com",
            "senna.helloworldopen.com",
            "webber.helloworldopen.com",
            "prost.helloworldopen.com"});
			this.HostComboBox.Location = new System.Drawing.Point(59, 12);
			this.HostComboBox.Name = "HostComboBox";
			this.HostComboBox.Size = new System.Drawing.Size(240, 21);
			this.HostComboBox.TabIndex = 1;
			// 
			// SimpleJoinRadioButton
			// 
			this.SimpleJoinRadioButton.AutoSize = true;
			this.SimpleJoinRadioButton.Location = new System.Drawing.Point(6, 19);
			this.SimpleJoinRadioButton.Name = "SimpleJoinRadioButton";
			this.SimpleJoinRadioButton.Size = new System.Drawing.Size(218, 17);
			this.SimpleJoinRadioButton.TabIndex = 0;
			this.SimpleJoinRadioButton.Text = "Join &single bot to any available race (join)";
			this.SimpleJoinRadioButton.UseVisualStyleBackColor = true;
			this.SimpleJoinRadioButton.CheckedChanged += new System.EventHandler(this.SimpleJoinRadioButton_CheckedChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 42);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(57, 13);
			this.label7.TabIndex = 4;
			this.label7.Text = "Bot &Name:";
			// 
			// BotNameTextBox
			// 
			this.BotNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BotNameTextBox.Location = new System.Drawing.Point(75, 39);
			this.BotNameTextBox.Name = "BotNameTextBox";
			this.BotNameTextBox.Size = new System.Drawing.Size(140, 20);
			this.BotNameTextBox.TabIndex = 5;
			this.BotNameTextBox.Text = "Ratty";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.CreateRaceRadioButton);
			this.groupBox1.Controls.Add(this.JoinRaceRadioButton);
			this.groupBox1.Controls.Add(this.SimpleJoinRadioButton);
			this.groupBox1.Location = new System.Drawing.Point(12, 80);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(386, 91);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Choose type of race:";
			// 
			// CreateRaceRadioButton
			// 
			this.CreateRaceRadioButton.AutoSize = true;
			this.CreateRaceRadioButton.Checked = true;
			this.CreateRaceRadioButton.Location = new System.Drawing.Point(6, 65);
			this.CreateRaceRadioButton.Name = "CreateRaceRadioButton";
			this.CreateRaceRadioButton.Size = new System.Drawing.Size(296, 17);
			this.CreateRaceRadioButton.TabIndex = 2;
			this.CreateRaceRadioButton.TabStop = true;
			this.CreateRaceRadioButton.Text = "&Create a race and join one or more bots to it (createRace)";
			this.CreateRaceRadioButton.UseVisualStyleBackColor = true;
			this.CreateRaceRadioButton.CheckedChanged += new System.EventHandler(this.CreateRaceRadioButton_CheckedChanged);
			// 
			// JoinRaceRadioButton
			// 
			this.JoinRaceRadioButton.AutoSize = true;
			this.JoinRaceRadioButton.Location = new System.Drawing.Point(6, 42);
			this.JoinRaceRadioButton.Name = "JoinRaceRadioButton";
			this.JoinRaceRadioButton.Size = new System.Drawing.Size(266, 17);
			this.JoinRaceRadioButton.TabIndex = 1;
			this.JoinRaceRadioButton.Text = "&Join one or more bots to an existing race (joinRace)";
			this.JoinRaceRadioButton.UseVisualStyleBackColor = true;
			this.JoinRaceRadioButton.CheckedChanged += new System.EventHandler(this.JoinRaceRadioButton_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.SameAsTotalCheckBox);
			this.groupBox2.Controls.Add(this.PrivateRaceCheckBox);
			this.groupBox2.Controls.Add(this.OurBotsLabel);
			this.groupBox2.Controls.Add(this.OurBotsUpDown);
			this.groupBox2.Controls.Add(this.TotalBotsLabel);
			this.groupBox2.Controls.Add(this.BotPositionUpDown);
			this.groupBox2.Controls.Add(this.TotalBotsUpDown);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.TrackNameLabel);
			this.groupBox2.Controls.Add(this.OtherDriverTypeComboBox);
			this.groupBox2.Controls.Add(this.DriverTypeComboBox);
			this.groupBox2.Controls.Add(this.TrackNameComboBox);
			this.groupBox2.Controls.Add(this.PasswordTextBox);
			this.groupBox2.Controls.Add(this.StartRaceButton);
			this.groupBox2.Location = new System.Drawing.Point(12, 177);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(386, 212);
			this.groupBox2.TabIndex = 9;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Configure race:";
			// 
			// SameAsTotalCheckBox
			// 
			this.SameAsTotalCheckBox.AutoSize = true;
			this.SameAsTotalCheckBox.Checked = true;
			this.SameAsTotalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.SameAsTotalCheckBox.Location = new System.Drawing.Point(225, 103);
			this.SameAsTotalCheckBox.Name = "SameAsTotalCheckBox";
			this.SameAsTotalCheckBox.Size = new System.Drawing.Size(90, 17);
			this.SameAsTotalCheckBox.TabIndex = 8;
			this.SameAsTotalCheckBox.Text = "Same as total";
			this.SameAsTotalCheckBox.UseVisualStyleBackColor = true;
			this.SameAsTotalCheckBox.CheckedChanged += new System.EventHandler(this.sameAsTotalCheckBox_CheckedChanged);
			// 
			// PrivateRaceCheckBox
			// 
			this.PrivateRaceCheckBox.AutoSize = true;
			this.PrivateRaceCheckBox.Checked = true;
			this.PrivateRaceCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.PrivateRaceCheckBox.Location = new System.Drawing.Point(9, 52);
			this.PrivateRaceCheckBox.Name = "PrivateRaceCheckBox";
			this.PrivateRaceCheckBox.Size = new System.Drawing.Size(138, 17);
			this.PrivateRaceCheckBox.TabIndex = 2;
			this.PrivateRaceCheckBox.Text = "Private race; &Password:";
			this.PrivateRaceCheckBox.UseVisualStyleBackColor = true;
			this.PrivateRaceCheckBox.CheckedChanged += new System.EventHandler(this.PrivateRaceCheckBox_CheckedChanged);
			// 
			// OurBotsLabel
			// 
			this.OurBotsLabel.AutoSize = true;
			this.OurBotsLabel.Location = new System.Drawing.Point(6, 104);
			this.OurBotsLabel.Name = "OurBotsLabel";
			this.OurBotsLabel.Size = new System.Drawing.Size(155, 13);
			this.OurBotsLabel.TabIndex = 6;
			this.OurBotsLabel.Text = "&Number of our bots to join race:";
			// 
			// OurBotsUpDown
			// 
			this.OurBotsUpDown.Location = new System.Drawing.Point(167, 102);
			this.OurBotsUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.OurBotsUpDown.Name = "OurBotsUpDown";
			this.OurBotsUpDown.Size = new System.Drawing.Size(52, 20);
			this.OurBotsUpDown.TabIndex = 7;
			this.OurBotsUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.OurBotsUpDown.ValueChanged += new System.EventHandler(this.OurBotsUpDown_ValueChanged);
			// 
			// BotPositionUpDown
			// 
			this.BotPositionUpDown.Location = new System.Drawing.Point(163, 155);
			this.BotPositionUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.BotPositionUpDown.Name = "BotPositionUpDown";
			this.BotPositionUpDown.Size = new System.Drawing.Size(52, 20);
			this.BotPositionUpDown.TabIndex = 5;
			this.BotPositionUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.BotPositionUpDown.ValueChanged += new System.EventHandler(this.BotPositionUpDown_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 184);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(120, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "&Other bot drivers to use:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 157);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(151, 13);
			this.label6.TabIndex = 9;
			this.label6.Text = "&Add main bot driver in position:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 131);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(112, 13);
			this.label9.TabIndex = 9;
			this.label9.Text = "Main bot &driver to use:";
			// 
			// OtherDriverTypeComboBox
			// 
			this.OtherDriverTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.OtherDriverTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.OtherDriverTypeComboBox.FormattingEnabled = true;
			this.OtherDriverTypeComboBox.Location = new System.Drawing.Point(132, 181);
			this.OtherDriverTypeComboBox.Name = "OtherDriverTypeComboBox";
			this.OtherDriverTypeComboBox.Size = new System.Drawing.Size(169, 21);
			this.OtherDriverTypeComboBox.TabIndex = 10;
			// 
			// DriverTypeComboBox
			// 
			this.DriverTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DriverTypeComboBox.FormattingEnabled = true;
			this.DriverTypeComboBox.Location = new System.Drawing.Point(124, 128);
			this.DriverTypeComboBox.Name = "DriverTypeComboBox";
			this.DriverTypeComboBox.Size = new System.Drawing.Size(177, 21);
			this.DriverTypeComboBox.TabIndex = 10;
			// 
			// PasswordTextBox
			// 
			this.PasswordTextBox.Location = new System.Drawing.Point(153, 50);
			this.PasswordTextBox.Name = "PasswordTextBox";
			this.PasswordTextBox.Size = new System.Drawing.Size(99, 20);
			this.PasswordTextBox.TabIndex = 3;
			this.PasswordTextBox.Text = "RatRacersOnly!";
			// 
			// MainForm
			// 
			this.AcceptButton = this.StartRaceButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(410, 429);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.HostComboBox);
			this.Controls.Add(this.PortUpDown);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.ReplayFilenameTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.BotNameTextBox);
			this.Controls.Add(this.BotKeyTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.StartReplayButton);
			this.Controls.Add(this.label1);
			this.Name = "MainForm";
			this.Text = "Rat Race UI";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.PortUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalBotsUpDown)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.OurBotsUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BotPositionUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartRaceButton;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox BotKeyTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown PortUpDown;
		private System.Windows.Forms.Button StartReplayButton;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox ReplayFilenameTextBox;
		private System.Windows.Forms.ComboBox TrackNameComboBox;
		private System.Windows.Forms.Label TrackNameLabel;
		private System.Windows.Forms.Label TotalBotsLabel;
		private System.Windows.Forms.NumericUpDown TotalBotsUpDown;
		private System.Windows.Forms.ComboBox HostComboBox;
		private System.Windows.Forms.RadioButton SimpleJoinRadioButton;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox BotNameTextBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton CreateRaceRadioButton;
		private System.Windows.Forms.RadioButton JoinRaceRadioButton;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label OurBotsLabel;
		private System.Windows.Forms.NumericUpDown OurBotsUpDown;
		private System.Windows.Forms.TextBox PasswordTextBox;
		private System.Windows.Forms.CheckBox SameAsTotalCheckBox;
		private System.Windows.Forms.CheckBox PrivateRaceCheckBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox DriverTypeComboBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox OtherDriverTypeComboBox;
		private System.Windows.Forms.NumericUpDown BotPositionUpDown;
		private System.Windows.Forms.Label label6;
    }
}

