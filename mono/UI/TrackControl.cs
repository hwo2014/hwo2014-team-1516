﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace mono.UI
{
	public partial class TrackControl : UserControl
	{
		private RaceModel	_raceModel;
		private int _tick;

		public RaceModel	RaceModel
		{
			get { return _raceModel; }

			set
			{
				if( _raceModel != null )
					_raceModel.ModelUpdated -= ModelUpdated;
				_raceModel = value;
				if( _raceModel != null )
					_raceModel.ModelUpdated += ModelUpdated;
			}
		}

		public string	Selected;

		public int Tick
		{
			get { return _tick; }
			set
			{
				if( _tick != value )
				{
					_tick = value;
					Invalidate();
				}
			}
		}

		public TrackControl()
		{
			InitializeComponent();
			
			ResizeRedraw = true;
		}

		protected override void OnPaint( PaintEventArgs e )
		{
			if( RaceModel == null || RaceModel.Pieces == null )
				return;

			var g = e.Graphics;

			var trackBounds = RaceModel.TrackBounds;

			var xScale = 0.95f * (Width  - 10) / trackBounds.Width;
			var yScale = 0.95f * (Height - 10) / trackBounds.Height;
			
			var scale = Math.Min(xScale, yScale);
			if( scale <= 0 )
				return;
			
			var saved = g.Save();
			g.TranslateTransform( Width / 2, Height / 2 );
			g.ScaleTransform( scale, -scale );
			g.TranslateTransform( -trackBounds.Width  / 2 - trackBounds.X,
								  -trackBounds.Height / 2 - trackBounds.Y );

			g.SmoothingMode = SmoothingMode.HighSpeed;

			var	pen = new Pen[]{ new Pen(Color.LightGray), new Pen(Color.Gray) };
			int p = 0;
			foreach( var piece in RaceModel.Pieces )
			{
				piece.Paint( g, pen[p], RaceModel.Lanes );
				p = 1 - p;
			}

			var cars = RaceModel.Cars;
			if( RaceModel.Ticks != null && Tick >= 0 && Tick < RaceModel.Ticks.Count )
			{
				cars = RaceModel.Ticks[Tick];
			}

			foreach( var car in cars )
			{
				car.Paint( g, car.Info.Name == Selected );
			}

			g.Restore( saved );
		}

		private void ModelUpdated( object source, EventArgs e )
		{
			Invalidate();
		}
	}
}
