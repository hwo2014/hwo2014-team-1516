﻿namespace mono.UI
{
    partial class RaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.tickTrackBar = new System.Windows.Forms.TrackBar();
			this.tickUpDown = new System.Windows.Forms.NumericUpDown();
			this.carInfoGroupBox = new System.Windows.Forms.GroupBox();
			this.infoTextBox = new System.Windows.Forms.TextBox();
			this.AddBotsTimer = new System.Windows.Forms.Timer(this.components);
			this.trackControl1 = new mono.UI.TrackControl();
			((System.ComponentModel.ISupportInitialize)(this.tickTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tickUpDown)).BeginInit();
			this.carInfoGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// tickTrackBar
			// 
			this.tickTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tickTrackBar.AutoSize = false;
			this.tickTrackBar.LargeChange = 100;
			this.tickTrackBar.Location = new System.Drawing.Point(170, 288);
			this.tickTrackBar.Maximum = 0;
			this.tickTrackBar.Name = "tickTrackBar";
			this.tickTrackBar.Size = new System.Drawing.Size(297, 32);
			this.tickTrackBar.TabIndex = 0;
			this.tickTrackBar.TickFrequency = 100;
			this.tickTrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
			this.tickTrackBar.Scroll += new System.EventHandler(this.tickTrackBar_Scroll);
			// 
			// tickUpDown
			// 
			this.tickUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.tickUpDown.Location = new System.Drawing.Point(473, 296);
			this.tickUpDown.Name = "tickUpDown";
			this.tickUpDown.Size = new System.Drawing.Size(57, 20);
			this.tickUpDown.TabIndex = 1;
			this.tickUpDown.ValueChanged += new System.EventHandler(this.tickUpDown_ValueChanged);
			// 
			// carInfoGroupBox
			// 
			this.carInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.carInfoGroupBox.Controls.Add(this.infoTextBox);
			this.carInfoGroupBox.Location = new System.Drawing.Point(12, 12);
			this.carInfoGroupBox.Name = "carInfoGroupBox";
			this.carInfoGroupBox.Size = new System.Drawing.Size(152, 308);
			this.carInfoGroupBox.TabIndex = 2;
			this.carInfoGroupBox.TabStop = false;
			this.carInfoGroupBox.Text = "Car Info:";
			// 
			// infoTextBox
			// 
			this.infoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.infoTextBox.Location = new System.Drawing.Point(3, 16);
			this.infoTextBox.Multiline = true;
			this.infoTextBox.Name = "infoTextBox";
			this.infoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.infoTextBox.Size = new System.Drawing.Size(146, 289);
			this.infoTextBox.TabIndex = 0;
			// 
			// AddBotsTimer
			// 
			this.AddBotsTimer.Interval = 20;
			this.AddBotsTimer.Tick += new System.EventHandler(this.AddBotsTimer_Tick);
			// 
			// trackControl1
			// 
			this.trackControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.trackControl1.Location = new System.Drawing.Point(170, 12);
			this.trackControl1.Name = "trackControl1";
			this.trackControl1.RaceModel = null;
			this.trackControl1.Size = new System.Drawing.Size(360, 270);
			this.trackControl1.TabIndex = 3;
			this.trackControl1.Tick = 0;
			// 
			// RaceForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(542, 332);
			this.Controls.Add(this.carInfoGroupBox);
			this.Controls.Add(this.tickUpDown);
			this.Controls.Add(this.tickTrackBar);
			this.Controls.Add(this.trackControl1);
			this.Name = "RaceForm";
			this.Text = "Race";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RaceForm_FormClosing);
			this.Load += new System.EventHandler(this.RaceForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.tickTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tickUpDown)).EndInit();
			this.carInfoGroupBox.ResumeLayout(false);
			this.carInfoGroupBox.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

		private TrackControl trackControl1;
		private System.Windows.Forms.TrackBar tickTrackBar;
		private System.Windows.Forms.NumericUpDown tickUpDown;
		private System.Windows.Forms.GroupBox carInfoGroupBox;
		private System.Windows.Forms.TextBox infoTextBox;
		private System.Windows.Forms.Timer AddBotsTimer;
    }
}