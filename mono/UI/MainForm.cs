﻿using System;
using System.IO;
using System.Windows.Forms;
using mono.Drivers;
using mono.Windows;

namespace mono.UI
{
    public partial class MainForm : Form
    {
		private readonly Type[]	_driverTypes = new []{	typeof(DumbDriver),
														typeof(HardcodedPieceSpeedsDriver),
														typeof(HardcodedWaypointsDriver),
														typeof(AutoWaypointDriver),
														typeof(SimLookaheadDriver1),
														typeof(SimLookaheadDriver2),
														typeof(SimLookaheadDriver3),
													};

		private readonly Props _props = new Props(	() => Properties.Settings.Default.ProgramSettings,
													x  => Properties.Settings.Default.ProgramSettings = x );

        public MainForm()
        {
            InitializeComponent();

			HostComboBox.SelectedIndex  = 1;
			TrackNameComboBox.SelectedIndex = 0;

			foreach( var driverType in _driverTypes )
			{
				DriverTypeComboBox.Items.Add( driverType.Name );
				OtherDriverTypeComboBox.Items.Add( driverType.Name );
			}

			DriverTypeComboBox.SelectedIndex = 5;
			OtherDriverTypeComboBox.SelectedIndex = 5;

			LoadSettings();
			UpdateControlStates();

            Log.Logger = new DebugLog();
            Log.JsonLogWriter = new StreamWriter(new FileStream("Race.txt", FileMode.Create));
            Log.Info( "Rat Race UI Started" );
        }

		private void LoadSettings()
		{
			HostComboBox.Text = _props.GetString("Host", HostComboBox.Text);
			PortUpDown.Text = _props.GetString("Port", PortUpDown.Text);
			BotNameTextBox.Text = _props.GetString("BotName", BotNameTextBox.Text);
			BotKeyTextBox.Text = _props.GetString("BotKey", BotKeyTextBox.Text);

			if( _props.GetBool("SimpleJoin", SimpleJoinRadioButton.Checked) )
				SimpleJoinRadioButton.Checked = true;
			else if( _props.GetBool("JoinRace", JoinRaceRadioButton.Checked) )
				JoinRaceRadioButton.Checked = true;
			else
				CreateRaceRadioButton.Checked = true;

			TrackNameComboBox.Text = _props.GetString("TrackName", TrackNameComboBox.Text);

			PrivateRaceCheckBox.Checked = _props.GetBool("PrivateRace", PrivateRaceCheckBox.Checked);
			PasswordTextBox.Text = _props.GetString("Password", PasswordTextBox.Text);

			TotalBotsUpDown.Text = _props.GetString("TotalBots", TotalBotsUpDown.Text);
			OurBotsUpDown.Text = _props.GetString("OurBots", OurBotsUpDown.Text);
			SameAsTotalCheckBox.Checked = _props.GetBool("SameAsTotal", SameAsTotalCheckBox.Checked);

			DriverTypeComboBox.Text = _props.GetString("DriverType", DriverTypeComboBox.Text);
			BotPositionUpDown.Text = _props.GetString("BotPosition", BotPositionUpDown.Text);
			OtherDriverTypeComboBox.Text = _props.GetString("OtherDriverType", OtherDriverTypeComboBox.Text);

			ReplayFilenameTextBox.Text = _props.GetString("ReplayFilename", ReplayFilenameTextBox.Text);
		}

		private void SaveSettings()
		{
			_props.SetString("Host", HostComboBox.Text);
			_props.SetString("Port", PortUpDown.Text);
			_props.SetString("BotName", BotNameTextBox.Text);
			_props.SetString("BotKey", BotKeyTextBox.Text);

			_props.SetBool("SimpleJoin", SimpleJoinRadioButton.Checked);
			_props.SetBool("JoinRace", JoinRaceRadioButton.Checked);

			_props.SetString("TrackName", TrackNameComboBox.Text);

			_props.SetBool("PrivateRace", PrivateRaceCheckBox.Checked);
			_props.SetString("Password", PasswordTextBox.Text);

			_props.SetString("TotalBots", TotalBotsUpDown.Text);
			_props.SetString("OurBots", OurBotsUpDown.Text);
			_props.SetBool("SameAsTotal", SameAsTotalCheckBox.Checked);

			_props.SetString("DriverType", DriverTypeComboBox.Text);
			_props.SetString("BotPosition", BotPositionUpDown.Text);
			_props.SetString("OtherDriverType", OtherDriverTypeComboBox.Text);

			_props.SetString("ReplayFilename", ReplayFilenameTextBox.Text);
		}

        private void StartRaceButton_Click(object sender, EventArgs e)
        {
			var track = TrackNameComboBox.Text;
			if( track.IndexOf(' ') >= 0 )
				track = track.Substring(0, track.IndexOf(' '));

			if( SimpleJoinRadioButton.Checked )
				track = null;

			var password = PasswordTextBox.Text;
			if( SimpleJoinRadioButton.Checked || !PrivateRaceCheckBox.Checked )
				password = null;

			SaveSettings();

			var ourBots = (int)OurBotsUpDown.Value;
			var driverTypes = new Type[ourBots];
			var mainBotIndex = (int)BotPositionUpDown.Value - 1;

			
			for( var i = 0; i < ourBots; i++ )
			{
				if( i == mainBotIndex )
					driverTypes[i] = _driverTypes[DriverTypeComboBox.SelectedIndex];
				else
					driverTypes[i] = _driverTypes[OtherDriverTypeComboBox.SelectedIndex];
			}

            var form = new RaceForm(HostComboBox.Text, int.Parse(PortUpDown.Text),
									BotNameTextBox.Text, BotKeyTextBox.Text,
									track, password, CreateRaceRadioButton.Checked,
									(int)TotalBotsUpDown.Value, ourBots,
									driverTypes, mainBotIndex);
            form.Show();
        }

        private void ReplayRaceButton_Click(object sender, EventArgs e)
        {
            var form = new RaceForm(ReplayFilenameTextBox.Text,
									_driverTypes[DriverTypeComboBox.SelectedIndex]);
            form.Show();
        }

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			FormUtils.SaveLocation( this );

			SaveSettings();

			Log.JsonLogWriter.Close();
			Log.JsonLogWriter = null;
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			FormUtils.RestoreLocation( this );
		}

		private void UpdateControlStates()
		{
			TrackNameComboBox.Enabled = !SimpleJoinRadioButton.Checked;
			TrackNameLabel.Enabled = !SimpleJoinRadioButton.Checked;
			PrivateRaceCheckBox.Enabled = !SimpleJoinRadioButton.Checked;
			PasswordTextBox.Enabled = PrivateRaceCheckBox.Checked && !SimpleJoinRadioButton.Checked;
			TotalBotsUpDown.Enabled = !SimpleJoinRadioButton.Checked;
			TotalBotsLabel.Enabled = !SimpleJoinRadioButton.Checked;
			OurBotsUpDown.Enabled = !SimpleJoinRadioButton.Checked && !SameAsTotalCheckBox.Checked;
			OurBotsLabel.Enabled = !SimpleJoinRadioButton.Checked && !SameAsTotalCheckBox.Checked;
			SameAsTotalCheckBox.Enabled = !SimpleJoinRadioButton.Checked;
		}

		private void PrivateRaceCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			UpdateControlStates();
		}

		private void sameAsTotalCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			UpdateControlStates();
			if( SameAsTotalCheckBox.Checked )
				OurBotsUpDown.Value = TotalBotsUpDown.Value;
		}

		private void SimpleJoinRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			UpdateControlStates();
		}

		private void JoinRaceRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			UpdateControlStates();
		}

		private void CreateRaceRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			UpdateControlStates();
		}

		private void TotalBotsUpDown_ValueChanged(object sender, EventArgs e)
		{
			if( SameAsTotalCheckBox.Checked ||
				OurBotsUpDown.Value > TotalBotsUpDown.Value )
			{
				OurBotsUpDown.Value = TotalBotsUpDown.Value;
			}
			if( OurBotsUpDown.Value < BotPositionUpDown.Value )
				BotPositionUpDown.Value = OurBotsUpDown.Value;
		}

		private void OurBotsUpDown_ValueChanged(object sender, EventArgs e)
		{
			if( OurBotsUpDown.Value > TotalBotsUpDown.Value )
				TotalBotsUpDown.Value = OurBotsUpDown.Value;
			if( OurBotsUpDown.Value < BotPositionUpDown.Value )
				BotPositionUpDown.Value = OurBotsUpDown.Value;
		}

		private void BotPositionUpDown_ValueChanged(object sender, EventArgs e)
		{
			if( BotPositionUpDown.Value > OurBotsUpDown.Value )
				OurBotsUpDown.Value = BotPositionUpDown.Value;
			if( BotPositionUpDown.Value > TotalBotsUpDown.Value )
				TotalBotsUpDown.Value = BotPositionUpDown.Value;
		}
    }
}
