﻿using System;
using System.Threading;
using System.Windows.Forms;
using mono.Windows;

namespace mono.UI
{
    public partial class RaceForm : Form
    {
        private Bot         _bot;
        private RaceModel   _raceModel = new RaceModel();

		private string		_botName;
    	private int			_ourBots;
    	private string		_password;
    	private Type[]		_driverTypes;
		private int			_mainBotIndex;

    	public RaceForm(
			string host, int port, string botName, string botKey,
			string track, string password, bool createRace,
			int raceBots, int ourBots, Type[] driverTypes, int mainBotIndex )
    	{
            InitializeComponent();

			// createRace
			_botName = botName;
            _bot = new Bot(host, port, track, 0, botName + "1", botKey,
						   password, createRace, raceBots, driverTypes[0], _raceModel);
            trackControl1.RaceModel = _raceModel;
			trackControl1.Selected = _botName + "1";

			_raceModel.KeepTicks = true;
			_raceModel.ModelUpdated += RaceModel_ModelUpdated;

            var thread = new Thread(_bot.run);
			thread.Name = _bot.BotName;
            thread.Start();

			if( ourBots > 1 )
			{
				// wait for bot to join a race before adding others...
				_ourBots = ourBots;
				_password = password;
				_driverTypes = driverTypes;
				_mainBotIndex = mainBotIndex;
				AddBotsTimer.Enabled = true;
			}
        }

    	public RaceForm( string host, int port, string botName, string botKey, Type driverType )
    	{
            InitializeComponent();

			// join
			_botName = botName;
            _bot = new Bot(host, port, null, 0, botName, botKey,
						   null, false, 0, driverType, _raceModel);
            trackControl1.RaceModel = _raceModel;

			_raceModel.KeepTicks = true;
			_raceModel.ModelUpdated += RaceModel_ModelUpdated;

            var thread = new Thread(_bot.run);
			thread.Name = _bot.BotName;
            thread.Start();

    	}

    	public RaceForm( string replayFilename, Type driverType )
        {
            InitializeComponent();
            
            _bot = new Bot(replayFilename, driverType, _raceModel);
            trackControl1.RaceModel = _raceModel;

			_raceModel.KeepTicks = true;
			_raceModel.ModelUpdated += RaceModel_ModelUpdated;

			carInfoGroupBox.Text = @"Car: " + _raceModel.MeName;

            var thread = new Thread(_bot.run);
            thread.Start();
        }

    	void RaceModel_ModelUpdated(object sender, EventArgs e)
		{
			var nTicks = _raceModel.Ticks == null ? 0 : _raceModel.Ticks.Count;
			var track = tickTrackBar.Value == tickTrackBar.Maximum;
			tickTrackBar.Maximum = nTicks;
			tickUpDown.Maximum = nTicks;

			if( track )
			{
				tickTrackBar.Value = nTicks;
				tickUpDown.Value = nTicks;
				trackControl1.Tick = tickTrackBar.Value;
			}

			UpdateStats();
		}

		private void UpdateStats()
		{
			var me = _raceModel.Me;
			if( me == null )
				return;

			//carInfoGroupBox.Text = "Car: " + me.Name;

			var tick = trackControl1.Tick;
			var millis = tick * 1000f / 60;

			var info = string.Format("Tick {0:d}\r\nMillis: {1:f}\r\n\r\n", tick, millis);

			if (tick >= 0 && tick < _raceModel.Ticks.Count)
				me = _raceModel.Ticks[tick][_raceModel.MeIndex];

			info += (me.Driver == null ? "No Driver" : me.Driver.GetType().Name) + "\r\n\r\n";

			info += string.Format("Last Lap: {0:d} ({1:f3})\r\n", me.LastLapTicks, me.LastLapTicks / 60.0 );
			info += string.Format("Best Lap: {0:d} ({1:f3})\r\n\r\n", me.BestLapTicks, me.BestLapTicks / 60.0 );

			info += string.Format("Lap:     {0:d}\r\n", me.Lap );
			info += string.Format("Index:   {0:d}\r\n", me.Index );
			info += string.Format("Frac:    {0:f3}\r\n", me.TrackPoint().Fraction );
			info += string.Format("Dist:    {0:f3}\r\n", me.Dist);
			info += string.Format("Dist2Go: {0:f3}\r\n\r\n", me.DistToGo());

			info += string.Format("Angle: {0:f6}\r\n", Utils.Degrees(-me.Angle));
			info += string.Format("AngSpd: {0:f6}\r\n", Utils.Degrees(-me.AngSpeed));
			info += string.Format("AngAcc: {0:f6}\r\n\r\n", Utils.Degrees(-me.AngAccel));

			info += string.Format("SimAng: {0:f6}\r\n", Utils.Degrees(-me.SimAngle));
			info += string.Format("SimAngSpd: {0:f6}\r\n\r\n", Utils.Degrees(-me.SimRotSpd));

			info += string.Format("AngErr: {0:f3}\r\n", Utils.Degrees(me.Angle - me.SimAngle));
			info += string.Format("AngErrMax: {0:f3}\r\n", Utils.Degrees(_raceModel.AngleErrorMax));
			info += string.Format("AngErrRMS: {0:f3}\r\n\r\n", Utils.Degrees(
								Math.Sqrt(_raceModel.AngleSumSqError / _raceModel.AngleSumSqErrCnt)));

			info += string.Format("Speed: {0:f3}\r\n", me.Speed);
			info += string.Format("Accel: {0:f3}\r\n", me.Accel);
			info += string.Format("LatAcc: {0:f6}\r\n", me.Speed * me.Speed * me.Curvature());
			info += string.Format("TargSpd: {0:f3}\r\n\r\n", me.TargetSpeed);

			info += string.Format("Curvature: {0:f4}\r\n", me.Curvature());
			info += string.Format("Radius: {0:f1}\r\n\r\n", me.Radius());

			info += string.Format("Throttle: {0:f6}\r\n\r\n", me.Throttle);

			info += _raceModel.DriftModel.GetUiString();

			var selStart = infoTextBox.SelectionStart;
			var selLen   = infoTextBox.SelectionLength;
			infoTextBox.Text = info;
			infoTextBox.Select(selStart, selLen);
		}

		private void tickTrackBar_Scroll(object sender, EventArgs e)
		{
			tickUpDown.Value = tickTrackBar.Value;
			trackControl1.Tick = tickTrackBar.Value;
			UpdateStats();
		}

		private void tickUpDown_ValueChanged(object sender, EventArgs e)
		{
			if( ContainsFocus )
			{
				tickTrackBar.Value = (int)tickUpDown.Value;
				trackControl1.Tick = tickTrackBar.Value;
				UpdateStats();
			}
		}

		private void RaceForm_Load(object sender, EventArgs e)
		{
			FormUtils.RestoreLocation( this );
		}

		private void RaceForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			FormUtils.SaveLocation( this );

			_bot.stop();
		}

		private void AddBotsTimer_Tick(object sender, EventArgs e)
		{
			AddBotsTimer.Enabled = false;

			// wait for main bot to join a race.
			while( !_bot.IsJoined )
				Thread.Sleep( 10 );

			// join some more bots...
			for( var i = 1; i < _ourBots; i++ )
			{
				// create extra bots...
				var rm = new RaceModel();
				var bot = new Bot(_bot.Host, _bot.Port, _bot.Track, i, _botName + (i + 1), _bot.BotKey,
								  _password, false, _bot.NumBots, _driverTypes[i], rm);

				if( i == _mainBotIndex )
				{
					_bot = bot;
					_raceModel.KeepTicks = false;
					_raceModel.ModelUpdated -= RaceModel_ModelUpdated;
					_raceModel = rm;
					_raceModel.KeepTicks = true;
					_raceModel.ModelUpdated += RaceModel_ModelUpdated;
					trackControl1.RaceModel = _raceModel;
					trackControl1.Selected = bot.BotName;
				}

				var thread = new Thread(bot.run);
				thread.Name = bot.BotName;
				thread.Start();

				// wait for bot to join a race.
				//Thread.Sleep( 100 );	// server seems to get confused if we don't to this.
				while( !bot.IsJoined )
					Thread.Sleep( 10 );
			}
		}
    }
}
