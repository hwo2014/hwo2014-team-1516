﻿using System;
using System.IO;

namespace mono
{
	public abstract class Log
	{
		public static Log Logger { get; set; }

		public static StreamWriter JsonLogWriter { get; set; }

		public virtual void DoInfo( string msg )
		{
		}
	
		public static void Info( string msg )
		{
			if( Logger != null )
			{
				var now = DateTime.Now;
			
				Logger.DoInfo(now.ToString("yyyy/MM/dd HH:mm:ss.ff - ") + msg);
			}
		}
	
		public static void Json( string line )
		{
			if( JsonLogWriter != null )
			{
				lock( JsonLogWriter )
				{
					JsonLogWriter.WriteLine( line );
					JsonLogWriter.Flush();
				}
			}
		}
	}
}
