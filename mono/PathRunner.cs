﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace mono
{
	public class PathRunner
	{
		RaceModel		_rm;
		Path			_path;
		List<ApexInfo>	_apexes = new List<ApexInfo>();
		List<WayPoint>	_waypoints =  new List<WayPoint>();

		public PathRunner( Path path )
		{
			_rm   = path.RaceModel;
			_path = path;

			Initialise();
		}

		public double CalcTargetSpeed( TrackPoint pos, SpeedModel sm )
		{
			int wpIndex = 0;
			while( wpIndex < _waypoints.Count && pos.IsAfter(_waypoints[wpIndex].Tp) )
				wpIndex++;
			if( wpIndex >= _waypoints.Count )
				wpIndex = 0;
			var wp = _waypoints[wpIndex];
			var targSpd = sm.TargetSpeed(_path.DistBetweenPoints(pos, wp.Tp), wp.Speed);

			return targSpd;
		}

		public double StraightDist( TrackPoint pos )
		{
			var pieces = _rm.Pieces;
			if( !pieces[pos.Index].IsStraight )
				return 0;

			double dist = _path.Dist(pos.Index) * (1 - pos.Fraction);

			for( int i = (pos.Index + 1) % pieces.Count; pieces[i].IsStraight; i = (i + 1) % pieces.Count )
			{
				dist += _path.Dist(i);
			}

			return dist;
		}

		// create apexes and initial waypoints.
		// start running simulator around laps.
		// after each lap, analyse the bends and make changes.
		// stop after #laps or all good enough down.
		// try running with the real thing.

		private bool IsMoreCurved( double k1, double k2 )
		{
			if( k1 == 0 )
				return k2 != 0;

			if( k1 > 0 )
				return k2 > k1;
			else
				return k2 < k1;
		}

		private bool IsMoreOrEquallyCurved( double k1, double k2 )
		{
			if( k1 == k2 )
				return true;

			return IsMoreCurved(k1, k2);
		}

		/// <summary>
		/// true if k2 is less curved than k1
		/// </summary>
		private bool IsLessCurved( double k1, double k2 )
		{
			// already minima?
			if( k1 == 0 )
				return false;

			// opposite signs?  doesn't count.
			if( k1 * k2 < 0 )
				return false;

			if( k1 > 0 )
				return k1 > k2;
			else
				return k1 < k2;
		}

		private bool IsLessOrEquallyCurved( double k1, double k2 )
		{
			if( k1 == k2 )
				return true;

			return IsLessCurved(k1, k2);
		}

		public double ThrottleControl( Car car, SpeedModel sm )
		{
			var targSpd = CalcTargetSpeed(car.TrackPoint(), sm);

			car.TargetSpeed = targSpd;
			return sm.ThrottleForSpeed(targSpd, car.Speed);
		}

		public class SectorInfo
		{
			public TrackPoint	Start;
			public TrackPoint	End;
			public Car			CarInit;

			public bool Contains( TrackPoint pt )
			{
				return pt.IsBetween(Start, End);
			}
		}

		public void Optimise( SpeedModel sm )
		{
			// hunt for fastest possible apex in corner.
			// hmmm. not possible to simulate backwards... but don't know where to start from before the apex.
			// try half way between apexes... need to run around the track to get some initial states.

			// divide the track up into sectors between the apexes.
			var sectors = new List<SectorInfo>();
			for( int i = 0; i < _apexes.Count; i++ )
			{
				int p = i > 0 ? i - 1 : _apexes.Count - 1;
				int n = i < _apexes.Count - 1 ? i + 1 : 0;
				sectors.Add( new SectorInfo{Start = _waypoints[p].Tp, End = _waypoints[n].Tp} );
			}

			var watch = new Stopwatch();
			watch.Start();

			var car = new Car(_rm);
			int sect = 1;
			var maxAng = 0.0;
			while( car.Lap != 2 )
			{
				_rm.Simulate( car, sm, ThrottleControl(car, sm) );

				maxAng = Math.Max(maxAng, Utils.Degrees(Math.Abs(car.Angle)));

				if( car.Lap > 0 && car.TrackPoint().IsBetween(sectors[sect].Start, _waypoints[sect].Tp) )
				{
					sectors[sect].CarInit = new Car(car);
					sect++;
					if( sect >= sectors.Count )
						sect = 0;
				}
			}

			watch.Stop();
			Log.Info( "Single lap simulation took: " + watch.Elapsed );

			// ok, now find best position for each apex.
			watch.Start();

			for( int c = 0; c < _apexes.Count * 3; c++ )
			{
				var i = c % _apexes.Count;

				var apex = _apexes[i];
				var wp = _waypoints[i];

				var bestSpd  = apex.Speed;
				var bestFrac = apex.Fraction;

				var best  = 99999;

				for(;;)
				{
					var bestF = -1.0;

					for( var frac = 0.0; frac <= 1.0; frac += (1.0/16) )
					{
						apex.Fraction = frac;
						wp.Tp = apex.Start.PartTo(apex.End, apex.Fraction, _rm.Pieces.Count);

						var sector = sectors[i];

						var sim = new Car(sectors[i].CarInit);
						var ticks = 0;
						var crashed = false;
						while( sector.Contains(sim.TrackPoint()) )
						{
							_rm.Simulate( sim, sm, ThrottleControl(sim, sm) );
							ticks++;
							if( Utils.Degrees(Math.Abs(sim.Angle)) > 45 )
							{
								crashed = true;
								break;
							}
						}

						if( !crashed && ticks <= best )
						{
							bestF = apex.Fraction;
							best  = ticks;
						}
					}

					if( bestF < 0 || wp.Speed >= 10 )
						break;

					bestSpd  = wp.Speed;
					bestFrac = bestF;

					wp.Speed += 0.1;
				}

				apex.Fraction = bestFrac;
				wp.Tp = apex.Start.PartTo(apex.End, bestFrac, _rm.Pieces.Count);
				wp.Speed = bestSpd - 0.1;
			}
/*
			_waypoints[0].Speed = 5.6;
//			_waypoints[1].Speed = 10;
			_waypoints[2].Speed = 4.9;
			_waypoints[3].Speed = 4.2;
//			_waypoints[4].Speed = 10;
			_waypoints[5].Speed = 6.4;
			_waypoints[6].Speed = 5.7;
			_waypoints[7].Speed = 4.2;
*/
			watch.Stop();
			Log.Info( "Optimisation took: " + watch.Elapsed );

			foreach( var wp in _waypoints )
			{
				Log.Info( "[" + wp.Tp + "] speed=" + wp.Speed );
			}
		}

		private void Initialise()
		{
			_waypoints.Clear();
			_apexes.Clear();

			// find start of 1st section...
			var pieces = _rm.Pieces;

			// find peaks in piece curvature values.
			// look for initial minima.
			var start = 0;
			var index = pieces.Count - 1;
			var minK = pieces[0].Curvature;
			while( index >= 0 && minK != 0 && IsLessCurved(minK, pieces[index].Curvature) )
			{
				minK = pieces[index].Curvature;
				index--;
				start = index;
			}

			if( index == 0 )
			{
				// ohoh, loop of continual curvature.
				throw new NotImplementedException("track loop");
			}

			index = start;

			var count  = 0;
			var minIdx = index;
			var maxIdx = index;
			while( count < pieces.Count )
			{
				// look for peak...
				var maxK = 0.0;
				while(	count < pieces.Count &&
						IsMoreOrEquallyCurved(maxK, pieces[index].Curvature) )
				{
					if( IsMoreCurved(maxK, pieces[index].Curvature) )
					{
						maxK = pieces[index].Curvature;
						maxIdx = index;
					}

					count++;
					index = (index + 1) % pieces.Count;
				}

				// found a peak, from maxIdx to index - 1...

				// conservative speed for currently known parameters
				var speed = Math.Sqrt(pieces[maxIdx].LaneRadius(_path.EndLane(maxIdx)) * 0.2);

				var apex = new ApexInfo{	Start = new TrackPoint(maxIdx, 0), End = new TrackPoint(index, 0), Fraction = 0.5,
											MinSpeed = 0, MaxSpeed = 10, Speed = speed };
				_apexes.Add( apex );
				_waypoints.Add( apex.CreateWayPoint(pieces.Count) );

				// look for minima...
				minK = maxK;
				while(	count < pieces.Count &&
						IsLessOrEquallyCurved(minK, pieces[index].Curvature) )
				{
					minK = pieces[index].Curvature;

					count++;
					index = (index + 1) % pieces.Count;
				}
			}
		}
	}
}
