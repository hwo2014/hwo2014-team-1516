﻿using System;
using System.Drawing;
using mono.Drivers;

namespace mono
{
	public enum Mode
	{
		None,
		Running,
		Crashed,
	};

	public class Car
	{
		public CarInfo	Info;

		public DriverBase Driver;

		public Mode		Mode;

		// dynamics from server.
		public int		Index;			/// Piece index.
		public double	Dist;			/// Distance within piece.
		public double	Angle;			/// Slip angle to lane.
		public int		StartLane;
		public int		EndLane;
		public int		Lap;

		public int		LastLapTicks;	/// last lap time (in ticks) for this car (0 if none).
		public int		BestLapTicks;	/// best lap time (in ticks) for this car so far, inc. qualifying.

		public int		TurboTicks;		/// number of ticks of turbo available.  0 if none.

		public double	Throttle;		/// throttle applied to get from state to the next state.
										/// if this car is not me, then this may be zero, or some posterior
										/// guess at the throttle setting.
		public double	Speed;			/// calculated from differences in distance within a segment.
		public bool		SpeedValid;		/// true if speed is up-to-date, else may be out of date (due to crossing a piece boundary.)
		public double	Accel;			/// calculated from differences in speeds within a segment.
		public double	AngSpeed;		/// rate of change of angle.
		public double	AngAccel;		/// rate of change of anglular speed.

		public double	TargetSpeed;	/// If any, the current speed target for info only.
		public int		AskedLane;		/// Asked to go to this lane.

		public double	SimAngle;		/// simulated angle of car.
		public double	SimRotSpd;		/// simulated rotational speed of car.
		public double	SimRotAcc;		/// simulated rotational acceleration of car.

		public bool		IsRunning { get { return Mode == Mode.Running; } }
		public bool		IsCrashed { get { return Mode == Mode.Crashed; } }

		public Car( RaceModel rm )
		{
			Info = new CarInfo(rm);
			LastLapTicks = 0;
			BestLapTicks = 0;
		}

		public Car( Car car )
		{
			Info		= car.Info;

			Mode		= car.Mode;

			Index		= car.Index;
			Dist		= car.Dist;
			Angle		= car.Angle;
			StartLane	= car.StartLane;
			EndLane		= car.EndLane;
			Lap			= car.Lap;

			LastLapTicks = car.LastLapTicks;
			BestLapTicks = car.BestLapTicks;

			Throttle	= car.Throttle;
			Speed		= car.Speed;
			Accel		= car.Accel;
			AngSpeed	= car.AngSpeed;
			AngAccel	= car.AngAccel;

			TargetSpeed = car.TargetSpeed;
			AskedLane	= car.AskedLane;

			SimAngle	= car.SimAngle;
			SimRotSpd	= car.SimRotSpd;
			SimRotAcc	= car.SimRotAcc;
		}

		public bool IsSameCar( Car other )
		{
			return Info.Name == other.Info.Name;
		}

		public void Paint( Graphics g, bool highlight )
		{
			var	b = new SolidBrush(Color.FromName(Info.Color));
			var pos = Info.RaceModel.CalcPosition(Index, Dist, StartLane, EndLane, Angle);
			var saved = g.Save();
			g.TranslateTransform( (float)pos[0], (float)pos[1] );
			g.RotateTransform( (float)Utils.Degrees(pos[2]) );
			g.FillRectangle( b, (float)(-Info.Length + Info.FlagPos), (float)(-Info.Width / 2),
			                 (float)Info.Length, (float)Info.Width );

			if( highlight )
			{
				g.TranslateTransform( (float)(-Info.Length / 2 + Info.FlagPos), 0 );
				g.FillEllipse( new SolidBrush(Color.White), (float)(-Info.Width * 0.3), (float)(-Info.Width * 0.3),
								(float)(Info.Width * 0.6), (float)(Info.Width * 0.6) );
			}

			g.Restore( saved );

			pos = Info.RaceModel.CalcPosition(Index, Dist, StartLane, EndLane, SimAngle);
			saved = g.Save();
			g.TranslateTransform( (float)pos[0], (float)pos[1] );
			g.RotateTransform( (float)Utils.Degrees(pos[2]) );
			g.DrawRectangle( new Pen(Color.Black),
							 (float)(-Info.Length + Info.FlagPos), (float)(-Info.Width / 2),
			                 (float)Info.Length, (float)Info.Width );
			g.Restore( saved );
		}

		public double Radius()
		{
			var piece = Info.RaceModel.Pieces[Index];
			if( piece.IsStraight )
				return 0;

			var radius = piece.LaneRadius(StartLane);
			if( piece.IsRight )
				radius = -radius;

			return radius;
		}

		public double Curvature()
		{
			var radius = Radius();
			if( radius == 0 )
				return 0;

			return 1.0 / radius;
		}

		public double DistToGo()
		{
			var piece = Info.RaceModel.Pieces[Index];
			return piece.Dist(StartLane, EndLane) - Dist;
		}

		public double DistFraction()
		{
			var piece = Info.RaceModel.Pieces[Index];
			return Dist / piece.Dist(StartLane, EndLane);
		}

		public TrackPoint TrackPoint()
		{
			var piece = Info.RaceModel.Pieces[Index];
			var fraction = Dist / piece.Dist(StartLane, EndLane);
			return new TrackPoint(Index, fraction);
		}
	}
}
