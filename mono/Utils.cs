﻿using System;

namespace mono
{
	public class Utils
	{
		public static double Radians( double degs )
		{
			return degs * Math.PI / 180;
		}

		public static double Degrees( double rads )
		{
			return rads * 180 / Math.PI;
		}

		public static void Swap<T>( ref T a, ref T b )
		{
			var t = a;
			a = b;
			b = t;
		}

		public static string ToString<T>( T[] a )
		{
			var s = string.Empty;
			var delim = "";
			foreach( var item in a )
			{
				s += delim + item;
				delim = " ";
			}
			return s;
		}
	}
}
