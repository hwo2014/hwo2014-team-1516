﻿using System;

namespace mono
{
	class ConsoleLog : Log
	{
		public override void DoInfo( string msg )
		{
			Console.WriteLine( msg );
		}
	}
}
