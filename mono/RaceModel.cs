﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace mono
{
    /// <summary>
    /// Class to hold the track and the race state.
    /// -- needs to be thread safe (perhaps have locking of some sort?)
    /// -- needs to be able to notify listening threads of applicable changes. DONE.
    /// 
    /// -- want to be able to:
    ///     *   work with track positions.  car positions from the server are specified
    ///         as segment/lane/distance.  also need to support x,y,angle and lane/total-distance.
    ///     *   keep track of bots speeds as well as positions.  DONE (in simplistic way).
    ///     *   perhaps to cut the track up into smaller segments, and keep info about
    ///         maximum speeds in each.  perhaps other values too, like grip.
    ///     *   for our bot, learn the relationship between the throttle and speed/acceleration.
    /// </summary>
    public class RaceModel
    {
		public string			TrackId		{ get; private set; }
		public string			TrackName	{ get; private set; }

		public int				RaceNumber	{ get; private set; }
		public bool				Qualifying	{ get; private set; }

		public Boolean			InGame		{ get; set; }
		public List<Piece>		Pieces		{ get; private set; }
		public double			TotalDist	{ get; private set; }
		public List<Car>		Cars		{ get; private set; }
		public List<Lane>		Lanes		{ get; private set; }
		public List<List<Car>>	Ticks		{ get; private set; }
		public RectangleF		TrackBounds { get; private set; }
		public List<Path>		LanePaths	{ get; private set; }
		public Path				TrackPath	{ get; private set; }
		public string			MeName		{ get; set; }
		public string			MeColor		{ get; set; }

		public bool				KeepTicks	{ get; set; }

		public Car				Me			{ get { return FindCar(MeName, MeColor); } }
		public int				MeIndex		{ get { return IndexOfCar(MeName, MeColor); } }

		public double			AngleSumSqError;
		public int				AngleSumSqErrCnt;
		public double			AngleErrorMax;

		public IDriftModel		BackupDriftModel = new SimpleDriftModel();
//		public IDriftModel		DriftModel = new RegressionDriftModel();
		public IDriftModel		DriftModel = new RevEngDriftModel();
//		public IDriftModel		DriftModel = new SimpleDriftModel();

//		private double[]		SimAccelCoeffs;

		public event EventHandler ModelUpdated;

    	public RaceModel()
		{
			InGame = false;
		}

		public double[] CalcPosition( int index, double dist, int startLane, int endLane, double angle )
		{
			var piece = Pieces[index];
			var lane1 = Lanes[startLane];
			var lane2 = Lanes[endLane];
			return piece.CalcPosition(dist, lane1.Offset, lane2.Offset, angle);
		}

		public void GameInit( JObject init )
		{
			if( init == null )
				return;

			InGame = false;

			Pieces	= new List<Piece>();
			Cars	= new List<Car>();
			Lanes	= new List<Lane>();
			Ticks	= KeepTicks ? new List<List<Car>>() : null;

			var race  = init["race"];
			var track = race["track"];

			TrackId		= (string)track["id"];
			TrackName	= (string)track["name"];

			double	x = 0;
			double	y = 0;
			double	angle = 0;	// towards the right.

			double	minX = x;
			double	minY = y;
			double	maxX = x;
			double	maxY = y;

			double	maxR = 0;

			// angles from the game get converted:
			//	game says +ve is to clockwise.
			//	model says +ve is anti-clockwise.
			//	game reports in Degrees.
			//	model stores in Radians.

			double totalDist = 0;

			foreach( var piece in track["pieces"] )
			{
				var p = new Piece(Pieces.Count, this);

				p.TotalDist = totalDist;
				p.StartX	= x;
				p.StartY	= y;
				p.StartAng	= angle;

				if( piece["length"] != null )
				{
					// straight
					p.Length = (double)piece["length"];
					p.Radius = 0;
					
					x += p.Length * Math.Cos(angle);
					y += p.Length * Math.Sin(angle);
				}
				else
				{
					// curve.
					p.Length = -Utils.Radians((double)piece["angle"]);
					p.Radius = (double)piece["radius"];

					if( maxR < p.Radius )
						maxR = p.Radius;

					double ang = p.Length;
					do
					{
						double	a = Math.Min(Math.PI / 2, Math.Abs(ang));
						double	s = p.Radius * Math.Tan(a / 2);
						double	dx = s * (Math.Cos(angle) + Math.Cos(angle + p.Length));
						double	dy = s * (Math.Sin(angle) + Math.Sin(angle + p.Length));
						x += dx;
						y += dy;
						if( ang < 0 )
							a = -a;
						angle += a;
						ang -= a;
					}
					while( Math.Abs(ang) > 0.0001 );
				}
				
				p.EndX = x;
				p.EndY = y;

				minX = Math.Min(minX, x); 
				minY = Math.Min(minY, y); 
				maxX = Math.Max(maxX, x); 
				maxY = Math.Max(maxY, y); 

				// can switch.
				p.Switch = piece["switch"] != null;
				Pieces.Add( p );

				totalDist += p.Dist(0.0);
			}

			TotalDist = totalDist;

			TrackBounds = new RectangleF((float)minX, (float)minY, (float)(maxX - minX), (float)(maxY - minY));

			double maxLaneOffset = 0;
			foreach( var lane in track["lanes"] )
			{
				var l = new Lane();

				l.Offset = (double)lane["distanceFromCenter"];
				var idx  = (int)lane["index"];

				maxLaneOffset = Math.Max(maxLaneOffset, Math.Abs(l.Offset));

				while( idx >= Lanes.Count )
					Lanes.Add( null );
				Lanes[idx] = l;
			}
/*
			SimAccelCoeffs = new double[(int)((maxR + maxLaneOffset) / 10) + 1];
			for( int i = 0; i < SimAccelCoeffs.Length; i++ )
			{
				SimAccelCoeffs[i] = 0.045;
			}

			if( SimAccelCoeffs.Length >  4 )	SimAccelCoeffs[ 4] = 0.03502135232;
			if( SimAccelCoeffs.Length >  6 )	SimAccelCoeffs[ 6] = 0.043990582;
			if( SimAccelCoeffs.Length >  9 )	SimAccelCoeffs[ 9] = 0.05158278643;
			if( SimAccelCoeffs.Length > 11 )	SimAccelCoeffs[11] = 0.05337708905;
			if( SimAccelCoeffs.Length > 18 )	SimAccelCoeffs[18] = 0.06619396231;
			if( SimAccelCoeffs.Length > 19 )	SimAccelCoeffs[19] = 0.02560500483;
			if( SimAccelCoeffs.Length > 20 )	SimAccelCoeffs[20] = 0.04499995691;
			if( SimAccelCoeffs.Length > 21 )	SimAccelCoeffs[21] = 0.03650179087;
*/
			int id = 0;
			foreach( var car in race["cars"] )
			{
				var c = new Car(this);
				
				c.Info.Id		= id++;
				c.Info.Name		= (string)car["id"]["name"];
				c.Info.Color	= (string)car["id"]["color"];
				c.Info.Length	= (double)car["dimensions"]["length"];
				c.Info.Width	= (double)car["dimensions"]["width"];
				c.Info.FlagPos	= (double)car["dimensions"]["guideFlagPosition"];

				c.Angle		= 0;
				c.Index		= 0;
				c.Dist		= 0;
				c.StartLane = 0;
				c.EndLane	= 0;
				c.Lap		= -1;

				c.Speed		= 0;
				c.Accel		= 0;
				c.AskedLane = 0;

				Cars.Add( c );
			}

			Log.Info( "Track has " + Pieces.Count + " pieces" );
			Log.Info( "Track has " + Lanes.Count + " lanes" );
			Log.Info( "Race has "  + Cars.Count + " cars" );

			LanePaths = new List<Path>();
			for( int i = 0; i < Lanes.Count; i++ )
				LanePaths.Add( new Path(this, i) );
			TrackPath = LanePaths[0];

			var session = race["raceSession"];
			RaceNumber++;
			//Qualifying = session["durationMs"] != null;
			Qualifying = session["laps"] == null;

			InvokeModelUpdated( new EventArgs() );
		}

		public void CarPositions( JArray positions, int gameTick, SpeedModel sm )
		{
			var tick = Ticks != null ? new List<Car>() : null;

			foreach( var pos in positions )
			{
				var name  = (string)pos["id"]["name"];
				var color = (string)pos["id"]["color"];

				var car = FindCar(name, color);
				if( car == null )
					// !!!
					continue;

				var newIndex = (int)pos["piecePosition"]["pieceIndex"];
				var newDist  = (double)pos["piecePosition"]["inPieceDistance"];

				var newAngle	= -Utils.Radians((double)pos["angle"]);
				var newRotVel	= newAngle  - car.Angle;
				var newRotAcc	= newRotVel - car.AngSpeed;

				// apply the drift calc with the old values, to see how well the sim
				// calculates the new ones.
				if( !DriftModel.Apply(
								car.Index, car.Radius(), car.Speed,
								ref car.SimAngle, ref car.SimRotSpd, ref car.SimRotAcc) )
				{
					BackupDriftModel.Apply(
									car.Index, car.Radius(), car.Speed,
									ref car.SimAngle, ref car.SimRotSpd, ref car.SimRotAcc);
				}


				if( car.Lap >= 2 && car.Index == 39 )
					car.SimAngle *= 1;
/*
				if( car.IsRunning )
				{
					Log.Info( string.Format("[{0:d}/{1:d}] ({2:f9}, {3:f9}, {4:f9}, {5:f9}) -> {6:f9}",
							gameTick, newIndex, car.AngSpeed, car.Angle, car.Speed * car.Speed, car.Speed, newRotAcc) );
				}
*/
				if( car == Me )
					DriftModel.NewData( gameTick, car, newIndex, newRotAcc );
/*
				if( car.Index == newIndex )
				{
					if( DriftIndex < 0 )
						DriftIndex++;
					else if( DriftIndex == 0 && car.Angle == 0 && car.AngSpeed == 0 )
					{
						// empty on purpose
					}
					else if( DriftIndex < 4 )
					{
						// want the old speed / angle data, but the new ang-accel data.
						DriftData.Set( DriftIndex, 0, car.AngSpeed );
						DriftData.Set( DriftIndex, 1, car.Angle );
						DriftData.Set( DriftIndex, 2, car.Speed );
						DriftData.Set( DriftIndex, 3, car.Speed * car.Speed );

						DriftTarget[DriftIndex] = newRotAcc;
						DriftIndex += 1;
					}

					var speed = newDist - car.Dist;
					car.Accel = speed - car.Speed;
					car.Speed = speed;
				}
				else
				{
					DriftIndex = -1;
				}
*/
				if( car.Index == newIndex )
				{
					var speed = newDist - car.Dist;
					car.Accel = speed - car.Speed;
					car.Speed = speed;
					car.SpeedValid = true;
				}
				else if( car.StartLane == car.EndLane )
				{
					var pieceDist = Pieces[car.Index].Dist(car.EndLane);
					var speed = newDist + pieceDist - car.Dist;
					car.Accel = speed - car.Speed;
					car.Speed = speed;
					car.SpeedValid = true;
				}
				else
				{
					// TODO: at some point need to calc correct speeds across piece boundaries after crossing lanes.
					if( car == Me )
					{
						var speedFromThrottle = sm.ApplyThrottle(car.Speed, car.Throttle);
						var pieceLength = car.Dist + speedFromThrottle - newDist;
						Log.Info( "[" + car.Index + "] length=" + pieceLength + " (l1=" + car.StartLane +
								  " l2=" + car.EndLane + " " + Pieces[car.Index].ToString(car.StartLane, car.EndLane) + ")" );
					}

					car.SpeedValid = false;
				}

				var angleError = car.SimAngle - car.Angle;

				if( car.Speed > 0 )
				{
					AngleSumSqError  += angleError * angleError;
					AngleSumSqErrCnt += 1;
					AngleErrorMax = Math.Max(AngleErrorMax, Math.Abs(angleError));
				}

				car.Angle		= newAngle;
				car.AngSpeed	= newRotVel;
				car.AngAccel	= newRotAcc;
				car.Index		= newIndex;
				car.Dist		= newDist;
				car.StartLane	= (int)pos["piecePosition"]["lane"]["startLaneIndex"];
				car.EndLane		= (int)pos["piecePosition"]["lane"]["endLaneIndex"];
				car.Lap			= (int)pos["piecePosition"]["lap"];
/*
				if( car.Speed > 0.5 &&
					(car.Angle != 0 || car.AngSpeed != 0) &&
					DriftIndex == 4 &&
					DriftModel.GetCoeffs(car.Index, car.Radius()) == null )
				{
					DriftModel.LearnCoeffs( car.Index, car.Radius(), DriftData, DriftTarget );
					var s = string.Empty;
					foreach( var c in DriftModel.GetCoeffs(car.Index, car.Radius()) )
					{
						s += string.Format("{0:g} ", c );
					}
					Log.Info( "[" + gameTick + "] coeffs: " + s );
					DriftIndex = 5;
				}
*/
				if( !InGame )
					car.AskedLane = car.EndLane;

				if( Ticks != null )
					tick.Add( new Car(car) );
			}

			if( Ticks != null )
				Ticks.Add( tick );

			InvokeModelUpdated( new EventArgs() );
		}

		public void GameStarted()
		{
			InGame = true;
			foreach( var car in Cars )
			{
				car.Mode = Mode.Running;
			}
		}

		public void GameEnded()
		{
			InGame = false;
			foreach( var car in Cars )
			{
				car.Mode = Mode.None;
			}

			DriftModel.DumpCoefficients();
		}

		public void LapFinisished( MsgWrapper msg )
		{
/*
			"msgType" : "lapFinished",
			"data" : {
			  "car": { "name": "Ratty1", "color": "red" },
			  "lapTime": { "lap": 1, "ticks": 289, "millis": 4816 },
			  "raceTime": { "laps": 2,  "ticks": 637, "millis": 10616 },
			  "ranking": { "overall": 1, "fastestLap": 1 }
			}
*/
			var name  = (string)((JObject)msg.data)["car"]["name"];
			var color = (string)((JObject)msg.data)["car"]["color"];
			var ticks = (int)((JObject)msg.data)["lapTime"]["ticks"];

			var car = FindCar(name, color);
			if( car != null )
			{
				car.LastLapTicks = ticks;
				if( car.BestLapTicks == 0 || car.BestLapTicks > ticks )
					car.BestLapTicks = ticks;
			}
  		}

		public void Crash( MsgWrapper msg )
		{
			var name  = (string)((JObject)msg.data)["name"];
			var color = (string)((JObject)msg.data)["color"];
			var car = FindCar(name, color);
			if( car != null )
			{
				car.Mode = Mode.Crashed;
				car.SimAngle = 0;
				car.SimRotSpd = 0;
				car.SimRotAcc = 0;
			}
		}

		public void Spawn( MsgWrapper msg )
		{
			var name  = (string)((JObject)msg.data)["name"];
			var color = (string)((JObject)msg.data)["color"];
			var car = FindCar(name, color);
			if( car != null )
			{
				car.Mode = Mode.Running;
			}
		}

		public void Simulate( Car car, SpeedModel sm, double throttle )
		{
			if( !DriftModel.Apply(
							car.Index, car.Radius(), car.Speed,
							ref car.Angle, ref car.AngSpeed, ref car.AngAccel) )
			{
				BackupDriftModel.Apply(
								car.Index, car.Radius(), car.Speed,
								ref car.Angle, ref car.AngSpeed, ref car.AngAccel);
			}

			// apply throttle...
			car.Speed = sm.ApplyThrottle(car.Speed, throttle);

			car.Dist += car.Speed;

			var pieceDist = Pieces[car.Index].Dist(car.StartLane, car.EndLane);
			if( car.Dist > pieceDist )
			{
				car.Dist -= pieceDist;
				car.Index = (car.Index + 1) % Pieces.Count;
				car.StartLane = car.EndLane;

				if( car.Index == 0 )
					car.Lap += 1;

				if( Pieces[car.Index].Switch && car.EndLane != car.AskedLane )
					car.EndLane = car.AskedLane;
			}
		}

		// sets the throttle value in the most recent Ticks entry, if any.
		public void SetLastThrottle( double throttle )
		{
			if( Ticks == null || Ticks.Count == 0 )
				return;

			try {
				Ticks[Ticks.Count - 1][MeIndex].Throttle = throttle;
			} catch( Exception ex ) {
				Log.Info( "SetLastThrottle(): " + ex.Message );
			}
		}

		public Car FindCar( string name, string color )
		{
			// todo: make this lookup faster.
			foreach( var car in Cars )
			{
				if( car.Info.Name == name && car.Info.Color == color )
					return car;
			}
			
			return null;
		}

		public int IndexOfCar(string name, string color)
		{
			// todo: make this lookup faster.
			for( int i = 0; i < Cars.Count; i++ )
			{
				var car = Cars[i];
				if( car.Info.Name == name && car.Info.Color == color )
					return i;
			}

			return -1;
		}

		public Piece NextCurve(Car car)
		{
			int index = car.Index;
			while (Pieces[index].IsStraight)
				index = (index + 1) % Pieces.Count;
			return Pieces[index];
		}

		public double AngleOfNextCurve(Car car)
		{
			int index = car.Index;
			while (Pieces[index].IsStraight)
				index = (index + 1) % Pieces.Count;
			bool isLeft = Pieces[index].IsLeft;
			double angle = 0;
			while( Pieces[index].IsCurve && Pieces[index].IsLeft == isLeft )
			{
				angle += Pieces[index].Length;
				index = (index + 1) % Pieces.Count;
			}
			return angle;
		}

		public double DistLeftOfCurrentCurve(Car car)
		{
			int index = car.Index;
			if( Pieces[index].IsStraight )
				return 0;

			double dist = car.DistToGo();
			int nextIndex = (index + 1) % Pieces.Count;
			int lane = car.EndLane;
			while( Pieces[index].IsCurveOfSameRadius(Pieces[nextIndex]) )
			{
				dist += Pieces[nextIndex].Dist(lane);
				index = nextIndex;
				nextIndex = (index + 1) % Pieces.Count;
			}
			return dist;
		}

		public Piece CurveAfterNextSwitch(Car car)
		{
			int index = car.Index;
			while( !Pieces[index].Switch )
				index = (index + 1) % Pieces.Count;
			while( Pieces[index].IsStraight )
				index = (index + 1) % Pieces.Count;
			return Pieces[index];
		}

		public double DistAfterNextSwitch( Car car, int lane )
		{
			int index = car.Index;
			while( !Pieces[index].Switch )
				index = (index + 1) % Pieces.Count;
			
			index = (index + 1) % Pieces.Count;
			double laneOffset = Lanes[lane].Offset;
			double dist = 0;
			while( !Pieces[index].Switch )
			{
				var p = Pieces[index];
				dist += p.IsStraight ? p.Length : Math.Abs(p.LaneRadius(laneOffset) * p.Length);
				index = (index + 1) % Pieces.Count;
			}

			return dist;
		}

		public double DistToNextCurve(Car car)
		{
			if( Pieces[car.Index].IsCurve )
				return 0;

			double	dist = Pieces[car.Index].Length - car.Dist;
			int index = (car.Index + 1) % Pieces.Count;
			while (Pieces[index].IsStraight)
			{
				dist += Pieces[index].Length;
				index = (index + 1) % Pieces.Count;
			}
			return dist;
		}

		public double NextRadius(Car car, int lane)
		{
			var piece = NextCurve(car);
			var offset = Lanes[lane].Offset;
			var radius = piece.Radius;
			radius += piece.Length > 0 ? offset : -offset;
			return radius;
		}

		public double TrackDist( double from, double to )
		{
			if( from > to )
				return TotalDist - from + to;
			else
				return to - from;
		}

		public double TrackDist( TrackPoint from, TrackPoint to )
		{
			return TrackPath.DistBetweenPoints(from, to);
		}

    	public void InvokeModelUpdated(EventArgs e)
    	{
    		var handler = ModelUpdated;
			if( handler == null )
				return;

			foreach( EventHandler singleCast in handler.GetInvocationList() )
			{
				var syncInvoke = singleCast.Target as ISynchronizeInvoke;
				try
				{
					if( null != syncInvoke && syncInvoke.InvokeRequired )
						syncInvoke.Invoke( singleCast, new object[] {this, e} );
					else
						singleCast (this, e );
				}
				catch( Exception ex )
				{
					Log.Info( "InvokeModelUpdated: " + ex.Message );
				}
			}
		}
	}
}