﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;

namespace mono
{
	public class Piece
	{
		public int			Index		{ get; private set; }
		public RaceModel	RaceModel	{ get; private set; }

		public double TotalDist	{ get; set; }	// of centre of track from start
		public double StartX	{ get; set; }	// x of centre-line at start.
		public double StartY	{ get; set; }	// y of centre-line at start.
		public double StartAng	{ get; set; }	// heading of centre-line at start.
	
		public double EndX		{ get; set; }
		public double EndY		{ get; set; }

		public double Length	{ get; set; }	// length if straight, else angle (radians, +ve is ACW).
		public double Radius	{ get; set; }	// 0 if straight, else radius.
		public bool   Switch	{ get; set; }	// true if can switch during this piece.

		public bool IsStraight	{ get { return Radius == 0; } }
		public bool IsCurve		{ get { return Radius != 0; } }
		public bool IsLeft		{ get { return IsCurve && Length > 0; } }
		public bool IsRight		{ get { return IsCurve && Length < 0; } }

		public double SignedRadius
		{
			get
			{
				if( Radius == 0 )
					return 0;

				return Length < 0 ? -Radius : Radius;
			}
		}

		public double Curvature		// this is signed
		{
			get
			{
				if( Radius == 0 )
					return 0;

				return Length < 0 ? -1 / Radius : 1 / Radius;
			}
		}

		public Piece( int index, RaceModel rm )
		{
			Index     = index;
			RaceModel = rm;
		}

		public TrackPoint	TrackPoint( double fraction )
		{
			return new TrackPoint(Index, fraction);
		}

		public double LaneRadius( int lane )
		{
			var offset = RaceModel.Lanes[lane].Offset;
			return LaneRadius(offset);
		}

		public double LaneRadius(double laneOffset)
		{
			return IsLeft ? Radius + laneOffset : Radius - laneOffset;
		}

		public double Dist( int lane )
		{
			if( IsStraight )
				return Length;

			double dist = LaneRadius(lane) * Length;
			return Math.Abs(dist);
		}

		public double Dist( int lane1, int lane2 )
		{
			if( IsStraight )
				return Length;

			// length=102.260274992934 (l1=0 l2=1 length=100)
			// length=81.0546522063749 (l1=1 l2=0 radius=190.., angle=-22.5

			double rad1 = LaneRadius(lane1);
			double rad2 = LaneRadius(lane2);
			double dist = (rad1 + rad2) * Length / 2;
			return Math.Abs(dist);
		}

		public double Dist( double offset1, double offset2 )
		{
			if( IsStraight )
				return Length;

			double rad1 = LaneRadius(offset1);
			double rad2 = LaneRadius(offset2);
			double dist = (rad1 + rad2) * Length / 2;
			return Math.Abs(dist);
		}

		public double Dist( double offset )
		{
			return Dist(offset, offset);
		}

		public bool IsCurveOfSameRadius( Piece curve )
		{
			Debug.Assert( IsCurve );

			return	curve.IsCurve &&
			      	Radius == curve.Radius &&
			      	IsLeft == curve.IsLeft;
		}

		public void Paint(Graphics g, Pen p, double offset)
		{
			if( IsStraight )
			{
				var	dx = offset * Math.Cos(StartAng - Math.PI / 2);
				var	dy = offset * Math.Sin(StartAng - Math.PI / 2);
				var	x1 = StartX + dx;
				var	y1 = StartY + dy;
				var	x2 = EndX + dx;
				var	y2 = EndY + dy;
				g.DrawLine(p, (float)x1, (float)y1, (float)x2, (float)y2);
			}
			else
			{
				var	cr = Length > 0 ? Radius : -Radius;
				var	cx = StartX + cr * Math.Cos(StartAng + Math.PI / 2);
				var	cy = StartY + cr * Math.Sin(StartAng + Math.PI / 2);
				var	r = Math.Abs(cr + offset);
				var	a = StartAng + (Length > 0 ? -Math.PI / 2 : Math.PI / 2);
				g.DrawArc(p, (float)(cx - r), (float)(cy - r), (float)(r * 2), (float)(r * 2),
				          (float)Utils.Degrees(a), (float)Utils.Degrees(Length));
			}
		}

		public void Paint( Graphics g, Pen p, List<Lane> lanes )
		{
			foreach( var lane in lanes )
			{
				Paint( g, Switch ? new Pen(Color.Magenta) : p, lane.Offset );
			}
		}

		public double[] CalcPosition(double dist, double startOffset, double endOffset, double angle)
		{
			if( IsStraight )
			{
				var t = dist / Length;
				var offset = startOffset + (endOffset - startOffset) * t;
				var	dx = offset * Math.Cos(StartAng - Math.PI / 2);
				var	dy = offset * Math.Sin(StartAng - Math.PI / 2);
				var	x = StartX + dx + dist * Math.Cos(StartAng);
				var	y = StartY + dy + dist * Math.Sin(StartAng);
				return new []{x, y, StartAng + angle};
			}
			else
			{
				var	cr = Length > 0 ? Radius : -Radius;
				var pieceDist = Dist(startOffset, endOffset);
//			var t = dist / Math.Abs(Length * (cr + offset));
				var t = dist / pieceDist;
				var offset = startOffset + (endOffset - startOffset) * t;
				var	cx = StartX + cr * Math.Cos(StartAng + Math.PI / 2);
				var	cy = StartY + cr * Math.Sin(StartAng + Math.PI / 2);
				var	r = Math.Abs(cr + offset);
				var	la = StartAng + Length * t;
				var ca = la + (Length > 0 ? -Math.PI / 2 : Math.PI / 2);
				var x = cx + r * Math.Cos(ca);
				var y = cy + r * Math.Sin(ca);
				return new []{x, y, la + angle};
			}
		}

		public override string ToString()
		{
			if( Radius == 0 )
				return "length=" + Length;

			return "radius=" + Radius + ", angle=" + Utils.Degrees(Length);
		}

		public string ToString( int lane1, int lane2 )
		{
			if( lane1 == lane2 || Radius == 0)
				return ToString();

			return "radius=" + LaneRadius(lane1) + ".." + LaneRadius(lane2) + ", angle=" + Utils.Degrees(Length);
		}
	}
}
