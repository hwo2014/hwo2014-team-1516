﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mono
{
	/// <summary>
	/// A TrackPoint stores an unambiguous longitudinal track position.
	/// It does this by storing a piece index and a fraction from 0.0 (incl.) to 1.0 (excl.)
	/// </summary>
	public class TrackPoint
	{
		public int		Index;
		public double	Fraction;

		public TrackPoint()
		{
		}

		public TrackPoint( int index, double fraction )
		{
			Index		= index;
			Fraction	= fraction;
		}

		public bool IsBefore( TrackPoint other )
		{
			return	Index <  other.Index ||
					Index == other.Index && Fraction < other.Fraction;
		}

		public bool IsAfter( TrackPoint other )
		{
			return	Index >  other.Index ||
					Index == other.Index && Fraction > other.Fraction;
		}

		public bool IsBetween( TrackPoint tp1, TrackPoint tp2 )
		{
			if( tp1.Index < tp2.Index )
				return IsAfter(tp1) && IsBefore(tp2);
			else if( tp1.Index > tp2.Index )
				return IsAfter(tp1) || IsBefore(tp2);
			else if( tp1.Fraction < tp2.Fraction )
				return tp1.Fraction <= Fraction && Fraction < tp2.Fraction;
			else
				return tp1.Fraction <= Fraction || Fraction < tp2.Fraction;
		}

		public double RangeTo( TrackPoint other, int pieceCount )
		{
			if( Index == other.Index && Fraction <= other.Fraction )
				return other.Fraction - Fraction;

			if( Index < other.Index )
				return (other.Index + other.Fraction) - (Index + Fraction);
			else
				return pieceCount - (Index + Fraction) + (other.Index + other.Fraction);
		}

		public TrackPoint PlusRange( double range, int pieceCount )
		{
			var total = Index + Fraction + range;
			if( total >= pieceCount )
				total -= pieceCount;

			var index = (int)total;
			var fraction = total - index;
			return new TrackPoint(index, fraction);
		}

		public TrackPoint PartTo( TrackPoint other, double fraction, int pieceCount )
		{
			var range = RangeTo(other, pieceCount);
			return PlusRange(range * fraction, pieceCount);
		}

		public override string ToString()
		{
			return string.Format("{0:d}:{1:f3}", Index, Fraction);
		}
	}
}
