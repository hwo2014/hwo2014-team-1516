
import re
import math

pieces = []
lanes  = []

def convItem( item ):
    pattern = '"([^"]*)":([^}]*)'
    match = re.search(pattern, item)
    key = match.group(1)
    value = match.group(2)
    if value == 'true':
        value = True
    elif value == 'false':
        value = False
    else:
        value = float(value)
    return key, value

def convPiece( piece ):
    items = piece.split(',')
    return {convItem(item)[0] : convItem(item)[1] for item in items}

def pieceRadius( piece, lane ):
    if "length" in piece:
        return 0

    # arc piece.
    radius = piece["radius"]
    angle  = math.radians(piece["angle"])
    offset = lanes[lane]["distanceFromCenter"]

    if angle > 0:
        radius -= offset
    else:
        radius += offset

    return radius    

def pieceSignedRadius( piece, lane ):
    if "length" in piece:
        return 0

    # arc piece.
    radius = piece["radius"]
    angle  = math.radians(piece["angle"])
    offset = lanes[lane]["distanceFromCenter"]

    if angle < 0:
        radius = -radius;

    return radius - offset
    

def pieceLength( piece, lane1, lane2 ):
    global lanes

    # straight peice?
    if "length" in piece:
        if lane1 == lane2:
            return piece["length"]
        else:
            return piece["length"] * 1.0206;

    # arc piece.
    radius = pieceRadius(piece, lane1)
    if lane1 != lane2:
        radius = (radius + pieceRadius(piece, lane2)) / 2 * 1.032
    angle   = math.radians(piece["angle"])

    return abs(angle * radius)

def K( radius ):
    return 0 if radius == 0 else 1.0 / radius

dataFile = open("data.csv", "w")
#print >>dataFile, "speed", "latAcc", "angle", "omega"
print >>dataFile, ",".join(["index", "radius",
                            "angle", "omega", "v", "targRotAcc", "",
                            "omega", "angle*v", "v", "v^2" ])

#{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"Ratty1","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"73a8db0b-f595-48db-8855-38249407c483"}
#"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":4.124328639044966,\
#"lane":{"startLaneIndex":0,"endLaneIndex":0}
pattern = '"angle":([^,]*)[^{]*{"pieceIndex":([^,]*),"inPieceDistance":([^,]*),'
pattern += '"lane":{"startLaneIndex":([^,]*),"endLaneIndex":([^}]*)}'

curIndex    = -1
curLength   = 0
totalLen    = 0
lastLen     = 0

lastIndex   = -1;
lastRadius  = 0
lastDist    = 0
lastSpeed   = 0
lastAngle   = 0
lastRotSpd  = 0
lastRotAcc  = 0

#fn = "Race-Example-Json-Log.txt"
#fn = "usa@0.9-with-learning-on-lap-3.txt"
#fn = "Race-DumbDriver-keimola.txt"
fn = "Race-DumbDriver-germany.txt"
#fn = "bin/debug/Race.txt"

for line in file(fn):
    if "gameInit" in line:
        #print line
        pieces = re.search(r'"pieces":\[{([^]]*)\]', line).group(1)
        pieces = pieces.split("},{")
        pieces = [convPiece(p) for p in pieces]
        print pieces

        lanes = re.search(r'"lanes":\[{([^]]*)\]', line).group(1)
        lanes = lanes.split("},{")
        print lanes
        lanes = [convPiece(l) for l in lanes]
        print lanes

    match = re.search(pattern, line)
    if match:
        angle = float(match.group(1))
        index = int(match.group(2))
        dist  = float(match.group(3))
        lane1 = int(match.group(4))
        lane2 = int(match.group(5))
        #print angle, index, dist

        piece   = pieces[index]
        radius  = pieceRadius(piece, lane1)
        speed   = dist  - lastDist
        rotSpd  = angle - lastAngle
        rotAcc  = rotSpd - lastRotSpd

        if index != curIndex:
            speed     += curLength
            lastLen   =  totalLen
            totalLen  += curLength
            curLength =  pieceLength(piece, lane1, lane2)
            curIndex  =  index

        data = [lastIndex, lastRadius,
                -math.radians(lastAngle), -math.radians(lastRotSpd), lastSpeed, -math.radians(rotAcc), "",
                -math.radians(lastRotSpd), lastSpeed * -math.radians(lastAngle),
                lastSpeed, lastSpeed ** 2]
        print >>dataFile, ",".join([str(x) for x in data])

        lastIndex   = index;
        lastRadius  = pieceSignedRadius(piece, lane1)
        lastDist    = dist
        lastSpeed   = speed
        lastAngle   = angle
        lastRotSpd  = rotSpd
        lastRotAcc  = rotAcc

dataFile.close()
