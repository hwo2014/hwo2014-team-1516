﻿namespace mono
{
	public interface IDriftModel
	{
		/// <summary>
		/// Adds new data to the drift model, if it is one that can learn.
		/// </summary>
		/// <param name="newGameTick">the game tick</param>
		/// <param name="oldCar">the car to learn from... this is the state before the car has been update from
		///					  the Position message.  in particular, car.Index may noe be equal to newIndex, and the car's
		///					  speed may not be valid.</param>
		/// <param name="newIndex">this is the piece index of the new position</param>
		/// <param name="newRotAcc">this is the calculated rotational acceleration of the car from the previous and new
		///							position info.</param>
		void NewData( int newGameTick, Car oldCar, int newIndex, double newRotAcc );

		bool Apply( int piece, double radius, double speed,
					ref double angle, ref double rotSpd, ref double rotAcc );

		string GetUiString();

		void DumpCoefficients();

		bool GotData( int index, double radius, double speed );
	}
}