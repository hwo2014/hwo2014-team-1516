﻿using System;
using System.Diagnostics;

namespace mono
{
	class SimpleDriftModel : IDriftModel
	{
		private double _accelCoef    = 0.045;
		private double _frictionCoef = 0.001275;

		/// from analysing the data from initial angles after straights, at various speeds
		/// and with various radii, this equation has been derived:
		///
		/// angle = (3.7 / (R + 20) + 1/45) * V*V - 0.3 V
		///   
		/// for the data see docs/angular-rotation-analysis.xls

		private double	AngAccelDegrees( double R, double V )
		{
			Debug.Assert( R >  0 );
			Debug.Assert( V >= 0 );

			var angAcc = (3.7 / (R + 20) + 1.0/45) * V * V - 0.3 * V;
			return Math.Max(0, angAcc);
		}

		// can the coefficients be learned?
/*
		public void NewAccelData( int gameTick, Car car, int newIndex, double newRotAcc )
		{
			if( car.Speed > 0 )
			{
				var rotAccError = car.AngAccel - car.SimRotAcc;

				var radius = car.Radius();
//				var t = 0.001;	// rate of learning
				var t = 1;	// rate of learning
//				var delta = angleError * t;
//				var delta = angleError * angleError * t * Math.Sign(angleError);
				var delta = rotAccError * t;

				if( radius != 0 )
				{
					var coef = _accelCoef;//DriftModel.GetAccelCoef(car.Index, radius);
					if( car.Angle > 0 )
					{
//						Log.Info( string.Format("adjustinng accel ceof from {0:f4} to {1:f4} ang: {2:f4} rad: {3:f4} rotAccErr: {4:f4}",
//										coef, (coef - delta), car.Angle, radius, rotAccError) );
						coef -= delta;
					}
					else if( car.Angle < 0 )
					{
						coef += delta;
//						Log.Info( string.Format("adjustinng accel ceof from {0:f4} to {1:f4} ang: {2:f4} rad: {3:f4} rotAccErr: {4:f4}",
//										coef, (coef + delta), car.Angle, radius, rotAccError) );
					}
					//DriftModel.SetAccelCoef(car.Index, radius, coef);
					_accelCoef = coef;
				}
				else
				{
					var coef = _frictionCoef;//DriftModel.GetFrictionCoef(car.Index);
					if( car.Angle > 0 )
						coef += delta;
					else if( car.Angle < 0 )
						coef -= delta;
					//DriftModel.SetFrictionCoef(car.Index, coef);
					//_frictionCoef = coef;
				}
			}
		}
 */
		public void NewData( int gameTick, Car car, int newIndex, double newRotAcc )
		{
			if( car.Index != newIndex )
			{
				// don't have valid (speed, accel) data here.
				return;
			}

			// compare what actually happened to what we predict should happen, and adjust the coefficients?
			// or just do a regression?
			//var angAcc = (3.7 / (R + 20) + 1.0/45) * V * V - 0.3 * V;

			// model as: acc = A * v*v + B * v;
			// but the rotAcc we have been passed has also had friction applied, which we currently model
			//	as being proportional to speed * angle;
		}

		public bool Apply( int piece, double radius, double speed, ref double angle, ref double rotSpd, ref double rotAcc )
		{
			var curvature = radius == 0 ? 0 : 1 / radius;

			var latAcc = speed * speed * curvature;

			// variables affecting the rotation of the car:
			//	* friction torque related to angle of the side of the car to the
			//	  direction of motion.
			//  * lateral acceleration torque acting on the centre of mass.
			//	* rotational inertia/speed??
					
			// from tests it seems that theres no sliding force for lateral accelerations of less than 0.32
			//throttle = 0.5368;	// just slides,  90 rad, v = 5.356, a = 0.3202
			//throttle = 0.5935;	// just slides, 110 rad, v = 5.935, a = 0.3202
			//throttle = 0.3578;	// just slides,  40 rad, v = 3.578, a = 0.32005
			//throttle = 0.4382;	// just slides,  60 rad, v = 4.382, a = 0.32003
			if( latAcc > 0.32 )
				latAcc -= 0.32;
			else if( latAcc < -0.32 )
				latAcc += 0.32;
			else
				latAcc = 0;

			//var accTorque  = latAcc * Math.Cos(angle);
			var accTorque  = latAcc;// * angle;
			//var fricTorque = -speed * Math.Sin(angle);
			var fricTorque = -speed * angle;

			// best so far.
			//var KA = 0.06;
			//var KF = 0.00125;

			var R = radius;
			////var KA = 0.055;
			////var KA = 0.045;	// smaller for german, rad=40
			//var KA = GetAccelCoef(radius);	// smaller for german, rad=40
//			var KF = 0.00125;
			var KF = 0.001275;
//			var rotAcc = KA * accTorque + KF * fricTorque;

			rotAcc = 0.0;
			if( R > 0 )
				rotAcc = Utils.Radians(AngAccelDegrees(R, speed));
			else if( R < 0 )
				rotAcc = Utils.Radians(-AngAccelDegrees(-R, speed));
			rotAcc += KF * fricTorque;

			rotSpd += rotAcc;
			angle  += rotSpd;
			rotSpd *= 0.9;

			return true;
		}

		public string GetUiString()
		{
			var info = string.Empty;
			info += string.Format("AccelK:    {0:f6}\r\n", _accelCoef );
			info += string.Format("FrictionK: {0:f6}\r\n", _frictionCoef );
			return info;
		}

		public void DumpCoefficients()
		{
//			info += string.Format("AccelK:    {0:f6}\r\n", _accelCoef );
//			info += string.Format("FrictionK: {0:f6}\r\n", _frictionCoef );
		}

		public bool GotData( int index, double radius, double speed )
		{
			return true;
		}
	}
}